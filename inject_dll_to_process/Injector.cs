﻿using Microsoft.Win32.SafeHandles;
using System;
using System.Runtime.InteropServices;
using System.Text;

//https://habr.com/ru/company/icl_services/blog/324718/

namespace inject_dll_to_process
{
    public class Injector
    {
        public static void Inject(Int32 processId, String libraryPath)
        {
            using (var openedProcess = new SafeWaitHandle(
               existingHandle: Win32.OpenProcess(processAccess: ProcessAccessFlags.All,
                                                bInheritHandle: false,
                                                     processId: processId),
                   ownsHandle: true))
            {
                var loadLibraryAddress = Win32.GetProcAddress(library: Library.kernel32,
                                                                 proc: Library.LoadLibraryA);

                Int32 len = libraryPath.Length;
                IntPtr lenPtr = new IntPtr(len);
                UIntPtr uLenPtr = new UIntPtr((uint)len);

                IntPtr allocatedAddress = Win32.VirtualAllocEx(
                               hProcess: openedProcess.DangerousGetHandle(),
                              lpAddress: IntPtr.Zero,
                                 dwSize: lenPtr,
                       flAllocationType: AllocationType.Reserve | AllocationType.Commit,
                              flProtect: MemoryProtection.ReadWrite);

                Boolean written = Win32.WriteProcessMemory(
                               hProcess: openedProcess.DangerousGetHandle(),
                          lpBaseAddress: allocatedAddress,
                               lpBuffer: Encoding.ASCII.GetBytes(libraryPath),
                                  nSize: uLenPtr,
                 lpNumberOfBytesWritten: out var writedBytesCount);

                using (var threadId = new SafeWaitHandle(Win32.CreateRemoteThread(
                               hProcess: openedProcess.DangerousGetHandle(),
                     lpThreadAttributes: IntPtr.Zero,
                            dwStackSize: 0,
                         lpStartAddress: loadLibraryAddress,
                            lpParameter: allocatedAddress,
                        dwCreationFlags: 0,
                             lpThreadId: out var threadIdOut), ownsHandle: true))
                {

                }
            }
        }
    }
}
