﻿using System.Text;
using System.Threading;
using NLog;
using NLog.LayoutRenderers;

namespace Tests
{
    public class ScopedLayoutRenderer : LayoutRenderer
    {
        protected override void Append(StringBuilder builder, LogEventInfo logEvent)
        {
            builder.Append(ScopedContext.Indent);
        }
    }
    public static class ScopedContext
    {
        private static readonly AsyncLocal<StringBuilder> _prefix
             = new AsyncLocal<StringBuilder>();

        private static StringBuilder Builder => (_prefix.Value ?? (_prefix.Value = new StringBuilder()));

        public static void Enter()
        {
            Builder.Append('\t');
        }
        public static void Exit()
        {
            var sb = Builder;
            sb.Remove(sb.Length - 1, 1);
        }
        public static string Indent => Builder.ToString();
    }
}