using NUnit.Framework;
using System;
using System.CommandLine;
using System.CommandLine.Builder;
using System.CommandLine.Invocation;

namespace Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            var command = new Command(
               name: "add",
               description: "add desc",
                argument: new Argument<string>()
                {
                    Description = "param desc",
                    Name = "param name",
                });
            command.AddOption(new Option("-h"));

            var parser =
               new CommandLineBuilder()
                   .UseVersionOption()
                   .UseHelp(new[] { "-h", "--help" })
                   //.AddCommand(command)
                   .Build();

            //var root = new RootCommand("my program", symbols: new[] { cmd });
            //var result = root.Parse(new[] { "add" , "param1"});
            var result = parser.Parse(new[] { "--help" });
            Console.WriteLine(result);
            parser.InvokeAsync(result);
            Assert.AreEqual(0, result.Errors.Count);
        }
    }
}