﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Tests
{
    public partial class Tests
    {
        public class FakeStream : Stream
        {
            public override bool CanRead => true;

            public override bool CanSeek => throw new System.NotImplementedException();

            public override bool CanWrite => throw new System.NotImplementedException();

            public override long Length => throw new System.NotImplementedException();

            public override long Position { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }

            public override void Flush()
            {
                throw new System.NotImplementedException();
            }

            private readonly List<(Int32 timeout, byte[] data)> _buffers;
            public FakeStream(List<(Int32 timeout, byte[] data)> buffers)
            {
                _buffers = buffers ?? throw new ArgumentNullException(nameof(buffers));
            }

            public override int Read(byte[] buffer, int offset, int count)
            {
                if (count < 0)
                    throw new System.ArgumentOutOfRangeException(nameof(count));
                if (count == 0)
                    return 0;

                if (_buffers.Count == 0)
                    return 0;

                var buf = _buffers[0];
                if (buf.timeout > 0)
                {
                    System.Threading.Thread.Sleep(1000);
                }
                var read = Math.Min(buf.data.Length, count);

                Array.Copy(buf.data, 0, buffer, offset, read);

                if (buf.data.Length > read)
                {
                    var rest = buf.data.Length - read;
                    Array.Copy(buf.data, read, buf.data, 0, length: rest);
                    var data = buf.data;
                    Array.Resize(ref data, rest);
                    _buffers[0] = (0, data);
                }
                else
                {
                    _buffers.RemoveAt(0);
                }

                return read;

            }

            public override long Seek(long offset, SeekOrigin origin)
            {
                throw new System.NotImplementedException();
            }

            public override void SetLength(long value)
            {
                throw new System.NotImplementedException();
            }

            public override void Write(byte[] buffer, int offset, int count)
            {
                throw new System.NotImplementedException();
            }
        }
    }
}