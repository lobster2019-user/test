﻿using System;
using System.Diagnostics;
using System.Diagnostics.Tracing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Connections.Client;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace test_signalr_client
{
    class Program
    {
        static void testclient()
        {
            var http = new HttpClient();
            var r = http.GetStringAsync("http://127.0.0.1:6565/api/hello/127.0.0.1").Result;
            Console.WriteLine(r);
        }
        static void testclient2()
        {
            var http = new HttpClient();
            var r = http.GetStringAsync("http://localhost:6565/api/hello/localhost").Result;
            Console.WriteLine(r);
        }
        static void testclient3()
        {
            var http = new HttpClient();
            var r = http.GetStringAsync("http://rabbitmq:6565/api/hello/rabbitmq").Result;
            Console.WriteLine(r);
        }
        static async Task Main(string[] args)
        {
            //testclient();
            //testclient2();
            //testclient3();

            /*
            var es = typeof(System.Net.Sockets.Socket).Assembly.GetType("System.Net.NetEventSource");
            var name = System.Diagnostics.Tracing.EventSource.GetName(es);
            Console.WriteLine(name);

            var esDefault = (EventSource)es.GetProperty("Log",
                System.Reflection.BindingFlags.Static
                | System.Reflection.BindingFlags.Public)
                .GetValue(null);
                

            var sources = System.Diagnostics.Tracing.EventSource.GetSources().ToArray();
            Console.WriteLine(sources.Length);
            */
            //     Assembly.LoadFile(@"F:\Dev\gitlab\test\test_signalr_client\bin\Debug\netcoreapp2.2\System.Net.Http.dll");


            HttpClientHandler handler = new HttpClientHandler();
            handler.Proxy = new WebProxy("http://127.0.0.1:8888/", BypassOnLocal: false);
            //      var passed= handler.Proxy.IsBypassed(new Uri("http://127.0.0.1/"));
            //   Console.WriteLine($"passed: {passed}");


            //        var h = new HttpClient(handler);
            //   h.GetStringAsync("http://rabbitmq:6565/api/hello/blabla").Wait();

            //    var http = new HttpClient();
            //      var r = await http
            //        .GetStringAsync("http://127.0.0.1:6565/api/hello/blabla");
            //   Console.WriteLine(r);

            var builder = new HubConnectionBuilder()
           .WithUrl("http://rabbitmq:6565/hubs/hello")

           .ConfigureLogging(z =>
           {
               //z.AddConsole().SetMinimumLevel(LogLevel.Trace);
           });
            builder.Services.Configure<HttpConnectionOptions>(Options =>
            {
                Options.SkipNegotiation = true;
                Options.Transports = Microsoft.AspNetCore.Http.Connections.HttpTransportType.WebSockets;
                Options.Proxy = handler.Proxy;
                Options.HttpMessageHandlerFactory = (HttpMessageHandler o) =>
                {
                    //((HttpClientHandler)o).UseProxy = false;
                    return o;
                };
                Options.WebSocketConfiguration = o =>
                {
                    Console.WriteLine();
                    // o.Proxy = null;
                };
            });


            var connection = builder.Build();

            //Console.WriteLine(proxy);
            await connection.StartAsync();
            var ev = new AutoResetEvent(false);
            

            connection.On<string>("OnMessage", z =>
             {
                 Console.WriteLine("on message: {0}", z);
                 ev.Set();
             });
            //  var result = await connection.InvokeAsync<string>("Invoke", "hello-text", cancellationToken: CancellationToken.None);
            //    Console.WriteLine("on result: {0}", result);
            //  await connection.InvokeAsync("Invoke2", "hello-text12", cancellationToken: CancellationToken.None);
            await connection.SendAsync("Invoke", "hello-text34", cancellationToken: CancellationToken.None);
            Console.WriteLine("on result:");
            var t = new Stopwatch();
            t.Start();
            //ev.WaitOne();
            for (var j = 0; j < 3; ++j)
            {
                t.Restart();
                for (var i = 0; i < 10; ++i)
                {
                    //ev.Reset();
                    await connection.InvokeAsync<string>("Invoke", "hello-text", cancellationToken: CancellationToken.None);
                    //await connection.SendAsync("Invoke2", "hello-text34", cancellationToken: CancellationToken.None);
                    //ev.WaitOne();
                }
                t.Stop();
                Console.WriteLine($"all {t.ElapsedMilliseconds / 1000.0:0.00000}");
            }
            Console.Read();
            await Task.Delay(1000);
            await connection.DisposeAsync();
        }
    }
}
