﻿using System;
using System.Collections.Generic;

namespace Lobster.System.Collections.Generic
{
    public class PeekableEnumerable<T> : IPeekableEnumerable<T>
    {
        private readonly IEnumerable<T> _enumerable;

        public PeekableEnumerable(IEnumerable<T> enumerable)
        {
            _enumerable = enumerable ?? throw new ArgumentNullException(nameof(enumerable));
        }

        public IPeekableEnumerator<T> GetEnumerator()
            => _enumerable.GetEnumerator().AsPeekable();
    }
}