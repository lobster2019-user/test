﻿using System.Collections.Generic;
using global::System;

namespace Lobster.System.Collections.Generic
{

    public interface IBufferedEnumerator<T> : IEnumerator<T>
    {
        Int32 Position { get; set; }
        T this[Int32 index] { get; }
        bool TryPeek(out T result);
        Int32 TryPeek(Int32 srcOffset, Int32 count, T[] dst, Int32 dstOffset);
    }
}