using NUnit.Framework;
using System.Net.Http;

namespace Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void SystemProxyTest()
        {
            HttpSystemProxy.TryCreate(out var proxy);
            var link = "http://127.0.0.1:6565/";
            Assert.False(proxy.IsBypassed(new System.Uri(link)));
            Assert.IsNull(proxy.GetProxy(new System.Uri(link)));
            Assert.Pass();
        }
    }
}