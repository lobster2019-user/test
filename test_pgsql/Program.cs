﻿using LinqToDB.Data;
using System;
using LinqToDB;
using LinqToDB.Mapping;
using System.Linq;

namespace test_pgsql
{
  
    class Program
    {
        [Table("test_table")]
        public class TempTable
        {
            [Column("id", IsPrimaryKey = true, IsIdentity = true)]
            public Int64 id { get; set; }
        }
        static DataConnection Create()
        {
            return new DataConnection(
                    new LinqToDB.DataProvider.PostgreSQL.PostgreSQLDataProvider(),
                    "host=127.0.0.1;user id=test;password=test;database=testdb;enlist=true");
        }
        static void Main(string[] args)
        {


            DataConnection.WriteTraceLine = (s1, s2) =>
            {
                Console.WriteLine(s1, s2);
            };
            DataConnection.TurnTraceSwitchOn();

            using (var dc = Create())
            {
                dc.DropTable<TempTable>(throwExceptionIfNotExists: false);
            }


            using (var tr = new System.Transactions.TransactionScope())
            {
                using (var dc = Create())
                {
                    dc.CreateTable<TempTable>();
                    dc.Insert(new TempTable());
                }
            }
            using (var dc = Create())
            {
                Console.WriteLine(dc.GetTable<TempTable>().Count());
            }

            using (var dc = Create())
            {
                dc.DropTable<TempTable>(throwExceptionIfNotExists: false);
            }


            using (var dc = Create())
            {
                using (var tr = dc.BeginTransaction())
                {
                    dc.CreateTable<TempTable>();
                    dc.Insert(new TempTable());
                }

            }

            using (var dc = Create())
            {
                Console.WriteLine(dc.GetTable<TempTable>().Count());
            }

        }
    }
}
