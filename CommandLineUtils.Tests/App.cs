﻿using McMaster.Extensions.CommandLineUtils;
using McMaster.Extensions.CommandLineUtils.Abstractions;
using System;
using System.ComponentModel.DataAnnotations;

namespace Tests
{
    [Command("Add")]
    public class AddCmd
    {
        //[MyValidation]
        [Option(CommandOptionType.SingleValue,
             ShortName = "s",
             LongName = "long")
             ]
        public string Arg1 { get; set; }
        public void OnExecute()
        {
            Console.WriteLine($"{this.GetType().Name}:{nameof(OnExecute)} arg1:{Arg1}");
        }
    }
    //[Subcommand(typeof(AddCmd))]
    //[HelpOption("--help")]
    public class App
    {
        //[Required]
        //[MyValidation]
        [Option(CommandOptionType.SingleValue,
            ShortName ="s",
            LongName ="long")
            ]
        public string Arg1 { get; set; }
        [Required]
        [Option(CommandOptionType.MultipleValue,
            ShortName = "s2",
            LongName = "long2")
            ]
        public string[] Arg2 { get; set; }
        [Required]
        //[MyValidation]
        [Option(CommandOptionType.SingleValue,
            ShortName = "s3",
            LongName = "long3")
            ]
        public string Arg3 { get; set; }
        /*
        private ValidationResult OnValidate(ValidationContext context, CommandLineContext appContext)
        {
            return new ValidationResult("Failed");
        }
        */
        public void OnExecute()
        {
            Console.WriteLine($"{this.GetType().Name}:{nameof(OnExecute)} arg1:{Arg1}");
            Console.WriteLine($"{this.GetType().Name}:{nameof(OnExecute)} arg2:{(Arg2 == null ? "<null>" : string.Join(",",Arg2))}");
            Console.WriteLine($"{this.GetType().Name}:{nameof(OnExecute)} arg3:{(Arg3 == null ? "<null>" : string.Join(",", Arg3))}");
            //return 0;
        }
    }
}