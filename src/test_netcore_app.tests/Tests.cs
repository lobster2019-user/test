using NUnit.Framework;
using DryIoc;
using System;
using System.Diagnostics;
using FluentAssertions;
using System.Collections.Generic;

namespace Tests
{
    public class TrackingDisposable : IDisposable
    {
        public TrackingDisposable(Int32 i)
        {
            DebugLogger.Log($"~ctor {i}");
        }
        public static Int32 ConstructorIndex = 0;
        public static Int32 DestructorIndex = 0;
        public Int32 Value { get; set; } = ++ConstructorIndex;

        public void Dispose()
        {
            DebugLogger.Log($"~dispose {Value}");
            ++DestructorIndex;
        }
    }
    public class DebugLogger
    {
        public static List<String> Lines { get; set; } = new List<String>();
        public static void Log(string s)
        {
            Lines.Add(s);
            Debug.WriteLine(s);
        }
        public static string String()
        {
            return string.Join("\r\n", Lines);
        }
    }
    public class Tests
    {

        [SetUp]
        public void SetUp()
        {
            TrackingDisposable.ConstructorIndex = 0;
            TrackingDisposable.DestructorIndex = 0;
        }

        [Test]
        public void DryIocTest_Issue80()
        {
            


            var container = new Container();
            var id = 1000;
            container.RegisterDelegate<Int32>((c) => ++id);
            container.Register<TrackingDisposable>(reuse: Reuse.ScopedOrSingleton);
            var childContainer = container.OpenScope(trackInParent: false);


            Assert.AreEqual(1, container.Resolve<TrackingDisposable>().Value);
            Assert.AreEqual(1, container.Resolve<TrackingDisposable>().Value);
            Assert.AreEqual(1, container.Resolve<TrackingDisposable>().Value);
            Assert.AreEqual(3, TrackingDisposable.ConstructorIndex);

            Assert.AreEqual(4, childContainer.Resolve<TrackingDisposable>().Value);
            Assert.AreEqual(4, childContainer.Resolve<TrackingDisposable>().Value);
            Assert.AreEqual(4, childContainer.Resolve<TrackingDisposable>().Value);
            Assert.AreEqual(6, TrackingDisposable.ConstructorIndex);

            Assert.AreEqual(0, TrackingDisposable.DestructorIndex);

            DebugLogger.Log("extra calls #80 for scope; ignore IDisposable");
            Assert.Fail(DebugLogger.String());
        }
        [Test]
        public void DryIocTest_Issue81()
        {

            var container = new Container();
            var id = 1000;
            container.RegisterDelegate<Int32>((c) => ++id);
            container.Register<TrackingDisposable>(reuse: Reuse.Scoped);
            var childContainer = container.OpenScope(trackInParent: false);


            Assert.AreEqual(1, childContainer.Resolve<TrackingDisposable>().Value);
            Assert.AreEqual(1, childContainer.Resolve<TrackingDisposable>().Value);
            Assert.AreEqual(1, childContainer.Resolve<TrackingDisposable>().Value);
            Assert.AreEqual(3, TrackingDisposable.ConstructorIndex);

            var childContainer2 = container.OpenScope(trackInParent: false);

            Assert.AreEqual(4, childContainer2.Resolve<TrackingDisposable>().Value);
            Assert.AreEqual(4, childContainer2.Resolve<TrackingDisposable>().Value);
            Assert.AreEqual(4, childContainer2.Resolve<TrackingDisposable>().Value);
            Assert.AreEqual(6, TrackingDisposable.ConstructorIndex);

            DebugLogger.Log("extra calls #81 for scope; ignore IDisposable");
            Assert.AreEqual(0, TrackingDisposable.DestructorIndex);

            Assert.Fail(DebugLogger.String());
        }

        [Test]
        public void Test_Scope_2()
        {

        }

        [Test]
        public void Test_Scope_1()
        {
            var id = 1000;
           string s = "12345";

            var container = new DryIoc.Container();
            container.RegisterInstance<string>(s);
            container.RegisterDelegate<Int32>((c) => ++id);
            container.Register<TrackingDisposable>(reuse: Reuse.Scoped);
            container.Resolve<string>().Should().Be(s);

            var child = container.OpenScope();
            child.Resolve<TrackingDisposable>().Value.Should().Be(1);
            child.Resolve<TrackingDisposable>().Value.Should().Be(1);
            child.Resolve<TrackingDisposable>().Value.Should().Be(1);
            child.Resolve<string>().Should().Be(s);

            var child2 = child.OpenScope();

            child2.Resolve<TrackingDisposable>().Value.Should().Be(4);
            child2.Resolve<TrackingDisposable>().Value.Should().Be(4);
            child2.Resolve<TrackingDisposable>().Value.Should().Be(4);
            child2.Resolve<string>().Should().Be(s);

            var child3 = child2.OpenScope();

            child3.Resolve<TrackingDisposable>().Value.Should().Be(7);
            child3.Resolve<TrackingDisposable>().Value.Should().Be(7);
            child3.Resolve<TrackingDisposable>().Value.Should().Be(7);
            child3.Resolve<string>().Should().Be(s);

            child3.Dispose();

            child2.Resolve<string>().Should().Be(s);
            child2.Resolve<TrackingDisposable>().Value.Should().Be(4);
            child2.Resolve<TrackingDisposable>().Value.Should().Be(4);
            child2.Resolve<TrackingDisposable>().Value.Should().Be(4);

            child.Resolve<string>().Should().Be(s);
            child.Resolve<TrackingDisposable>().Value.Should().Be(1);
            child.Resolve<TrackingDisposable>().Value.Should().Be(1);
            child.Resolve<TrackingDisposable>().Value.Should().Be(1);

            container.Resolve<string>().Should().Be(s);

            var ms = container.With(rules => Rules.MicrosoftDependencyInjectionRules);
            var value = 6666;
            
            ms.UseInstance<Int32>(value);
            ms.Resolve<Int32>().Should().Be(value);
        }


        [Test]
        public void Test_Signalr()
        {

        }
    }
    

    /*
                 Assert.AreEqual(-777, childContainer.Resolve<Int32>());
            childContainer.Dispose();
            Assert.AreEqual(-777, container.Resolve<Int32>());
            Assert.Throws<ContainerException>(() => childContainer.Resolve<Int32>());
     */
}