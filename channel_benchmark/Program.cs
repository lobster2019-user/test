﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;
using System;
using System.Threading.Channels;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace channel_benchmark
{
    public class BenchmarkApp
    {
        //10_000, 
        //1_000_000
        //50 /1000/1000 * 100_000
        [Params(100_000)]
        public Int32 Capacity;
        private Channel<T> CreateBounded<T>(BoundedChannelFullMode mode)
        {
            return Channel.CreateBounded<T>(
                new BoundedChannelOptions(Capacity)
                {
                    SingleReader = true,
                    SingleWriter = true,
                    FullMode = mode,
                    AllowSynchronousContinuations = false,
                });
        }
        private Channel<T> CreateUnbounded<T>()
        {
            return Channel.CreateUnbounded<T>(
                new UnboundedChannelOptions()
                {
                    SingleReader = true,
                    SingleWriter = true,
                    AllowSynchronousContinuations = false,                    
                });
        }
        private void ReadAll<T>(Channel<T> channel)
        {
            var reader = channel.Reader;
            for (var i = 0; i < Capacity; ++i)
            {
                reader.TryRead(out var _);
            }
        }
        private void ReadAll<T>(BufferBlock<T> buffer)
        {
            for (var i = 0; i < Capacity; ++i)
            {
                buffer.TryReceive(out var _i);
            }
        }

        private async Task ReadAsyncAll<T>(Channel<T> channel)
        {
            var reader = channel.Reader;
            var i = 0;
            var j = 0;
            while (await reader.WaitToReadAsync())
            {
                //for (var i = 0; i < 2; ++i)
                {
                    var k = 0;
                    while (reader.TryRead(out var _))
                    {
                        ++i;
                        ++j;
                        ++k;
                        if (i == 5)
                        {
                            if (k < 100)
                            {
                                await Task.Yield();
                            }
                            i = 0;
                        }
                      //  if(k > 100)
                       // {
                        //    throw new ArgumentNullException(j.ToString());
                        //}
                    }
                }
            }
        }
        private async Task ReadAsyncAll<T>(BufferBlock<T> buffer)
        {
            for (var i = 0; i < Capacity; ++i)
            {
                await buffer.ReceiveAsync();
            }
        }

        public void Channel_WriteInt32(Channel<Int32> channel)
        {
            var writer = channel.Writer;
            for (var i = 0; i < Capacity; ++i)
            {
                writer.TryWrite(i);
            }
            writer.Complete();
        }
        public void Buffer_WriteInt32(BufferBlock<Int32> buffer)
        {
            for (var i = 0; i < Capacity; ++i)
            {
                buffer.Post(i);
            }
            buffer.Complete();
        }
        public void Channel_WriteInt64(Channel<Int64> channel)
        {
            var writer = channel.Writer;
            for (var i = 0; i < Capacity; ++i)
            {
                writer.TryWrite(i);
            }
            writer.Complete();
        }

        void Channel_WriteThenReadInt32(Channel<Int32> channel)
        {
            Channel_WriteInt32(channel);
            ReadAll(channel);
        }
        void Buffer_WriteThenReadInt32(BufferBlock<Int32> channel)
        {
            Buffer_WriteInt32(channel);
            ReadAll(channel);
        }
        Task Channel_WriteAndReadInt32(Channel<Int32> channel)
        {
            var task = ReadAsyncAll(channel);
            Channel_WriteInt32(channel);
            return task;
        }
        Task Buffer_WriteAndReadInt32(BufferBlock<Int32> channel)
        {
            var task = ReadAsyncAll(channel);
            Buffer_WriteInt32(channel);
            return task;
        }

        void Channel_WriteThenReadInt64(Channel<Int64> channel)
        {
            Channel_WriteInt64(channel);
            ReadAll(channel);
        }
        Task Channel_WriteAndReadInt64(Channel<Int64> channel)
        {
            var task = ReadAsyncAll(channel);
            Channel_WriteInt64(channel);
            return task;
        }


   //     [Benchmark]
        public void Channel_BoundedWriteInt32()
        {
            var channel = CreateBounded<Int32>(BoundedChannelFullMode.Wait);
            Channel_WriteInt32(channel);
        }

    //    [Benchmark]
        public void Channel_BoundedWriteThenReadInt32()
        {
            var channel = CreateBounded<Int32>(BoundedChannelFullMode.Wait);
            Channel_WriteThenReadInt32(channel);
        }

     //   [Benchmark]
        public Task Channel_BoundedWriteAndReadInt32()
        {
            var channel = CreateBounded<Int32>(BoundedChannelFullMode.Wait);
            return Channel_WriteAndReadInt32(channel);
        }


    //    [Benchmark]
        public void Channel_UnboundedWriteInt32()
        {
            var channel = CreateUnbounded<Int32>();
            Channel_WriteInt32(channel);
        }

   //     [Benchmark]
        public void Channel_UnboundedWriteReadInt32()
        {
            var channel = CreateUnbounded<Int32>();
            Channel_WriteThenReadInt32(channel);
        }

     //   [Benchmark]
        public Task Channel_UnboundedWriteAndReadInt32()
        {
            var channel = CreateUnbounded<Int32>();
            return Channel_WriteAndReadInt32(channel);
        }


        //   [Benchmark]
        public void Channel_BoundedWriteInt64()
        {
            var channel = CreateBounded<Int64>(BoundedChannelFullMode.Wait);
            Channel_WriteInt64(channel);
        }

        //      [Benchmark]
        public void Channel_BoundedWriteThenReadInt64()
        {
            var channel = CreateBounded<Int64>(BoundedChannelFullMode.Wait);
            Channel_WriteThenReadInt64(channel);
        }

            [Benchmark]
        public Task Channel_BoundedWriteAndReadInt64()
        {
            var channel = CreateBounded<Int64>(BoundedChannelFullMode.Wait);
            return Channel_WriteAndReadInt64(channel);
        }


        //      [Benchmark]
        public void Channel_UnboundedWriteInt64()
        {
            var channel = CreateUnbounded<Int64>();
            Channel_WriteInt64(channel);
        }

        //      [Benchmark]
        public void Channel_UnboundedWriteReadInt64()
        {
            var channel = CreateUnbounded<Int64>();
            Channel_WriteThenReadInt64(channel);
        }

        //      [Benchmark]
        public Task Channel_UnboundedWriteAndReadInt64()
        {
            var channel = CreateUnbounded<Int64>();
            return Channel_WriteAndReadInt64(channel);
        }


        #region dataflow

     //      [Benchmark]
        public void DataFlow_BoundedWriteInt32()
        {
            var channel = new BufferBlock<Int32>(new DataflowBlockOptions { BoundedCapacity=Capacity });
            Buffer_WriteInt32(channel);
        }
     //       [Benchmark]
        public void DataFlow_BoundedWriteThenReadInt32()
        {
            var channel = new BufferBlock<Int32>(new DataflowBlockOptions { BoundedCapacity = Capacity });
            Buffer_WriteThenReadInt32(channel);
        }

     //   [Benchmark]
        public void DataFlow_BoundedWriteAndReadInt32()
        {
            var channel = new BufferBlock<Int32>(new DataflowBlockOptions { BoundedCapacity = Capacity });
            Buffer_WriteAndReadInt32(channel);
        }


      //  [Benchmark]
        public void DataFlow_UnboundedWriteInt32()
        {
            var channel = new BufferBlock<Int32>(new DataflowBlockOptions { });
            Buffer_WriteInt32(channel);
        }
      //  [Benchmark]
        public void DataFlow_UnboundedWriteThenReadInt32()
        {
            var channel = new BufferBlock<Int32>(new DataflowBlockOptions { });
            Buffer_WriteThenReadInt32(channel);
        }

     //   [Benchmark]
        public void DataFlow_UnboundedWriteAndReadInt32()
        {
            var channel = new BufferBlock<Int32>(new DataflowBlockOptions { });
            Buffer_WriteAndReadInt32(channel);
        }

        //    [Benchmark]
        public void DataFlow_BoundedWriteOneToOneInt64()
        {
            var channel = new BufferBlock<Int64>(new DataflowBlockOptions
            {
                BoundedCapacity = Capacity
            }
             );
            for (var i = 0; i < Capacity; ++i)
            {
                channel.Post(i);
            }
        }
        //    [Benchmark]
        public void DataFlow_UnboundedWriteOneToOneInt64()
        {
            var channel = new BufferBlock<Int64>(new DataflowBlockOptions
            {

            }
              );
            for (var i = 0; i < Capacity; ++i)
            {
                channel.Post(i);
            }
        }

        #endregion
    }

    class Program
    {
        static void Main(string[] args)
        {
            BenchmarkRunner.Run<BenchmarkApp>();
        }
    }

}
