﻿using System.Collections;
using System.Collections.Generic;
using System;

namespace Lobster.System.Collections.Generic
{
    public class PeekableEnumerator<T, TEnumerator> : IPeekableEnumerator<T>
        where TEnumerator : IEnumerator<T>
    {
        private T _next = default(T);
        private T _current = default(T);
        private bool _hasNext = false;
        private bool _hasCurrent = false;
        private bool _disposed = false;

        public TEnumerator BaseEnumerator { get; }

        public PeekableEnumerator(TEnumerator enumerator)
        {
            if(enumerator == null)
            {
                throw new ArgumentNullException(nameof(enumerator));
            }
            BaseEnumerator = enumerator;
        }

        private void CheckDisposed()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(this.GetType().Name);
            }
        }

        private void ClearNext()
        {
            _hasNext = false;
            _next = default(T);
        }

        private void ClearCurrent()
        {
            _hasCurrent = false;
            _current = default(T);
        }

        public T Peek
        {
            get
            {
                CheckDisposed();
                if (!CanPeek)
                {
                    throw new InvalidOperationException();
                }
                return _next;
            }
        }

        public T Current
        {
            get
            {
                CheckDisposed();
                if (!_hasCurrent)
                {
                    throw new InvalidOperationException();
                }
                return _current;
            }
        }

        object IEnumerator.Current => this.Current;

        public void Dispose()
        {
            if (_disposed)
            {
                return;
            }
            CheckDisposed();
            _disposed = true;
            ClearCurrent();
            ClearNext();
            BaseEnumerator.Dispose();
        }

        public bool MoveNext()
        {
            CheckDisposed();
            if (_hasNext)
            {
                _hasCurrent = true;
                _current = _next;
                ClearNext();
                return true;
            }
            if (BaseEnumerator.MoveNext())
            {
                _hasCurrent = true;
                _current = BaseEnumerator.Current;
                return true;
            }
            ClearCurrent();
            return false;
        }

        public bool CanPeek
        {
            get
            {
                CheckDisposed();
                if (_hasNext)
                {
                    return true;
                }
                if (!BaseEnumerator.MoveNext())
                {
                    return false;
                }
                _hasNext = true;
                _next = BaseEnumerator.Current;
                return true;
            }
        }

        public void Reset()
        {
            CheckDisposed();
            ClearNext();
            ClearCurrent();
            BaseEnumerator.Reset();
        }
    }
}