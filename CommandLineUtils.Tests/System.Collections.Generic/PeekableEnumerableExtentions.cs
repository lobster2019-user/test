﻿using System.Collections.Generic;

namespace Lobster.System.Collections.Generic
{
    public static class PeekableEnumerableExtentions
    {
        public static IPeekableEnumerable<T> AsPeekable<T>(this IEnumerable<T> enumerable)
            => new PeekableEnumerable<T>(enumerable);

        public static IPeekableEnumerator<T> AsPeekable<T>(this IEnumerator<T> enumerator)
            => new PeekableEnumerator<T, IEnumerator<T>>(enumerator);

        public static PeekableEnumerator<T, TEnumerator> AsPeekable<T, TEnumerator>(this TEnumerator enumerator)
            where TEnumerator : IEnumerator<T>
            => new PeekableEnumerator<T, TEnumerator>(enumerator);
    }
}