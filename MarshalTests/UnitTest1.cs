using EasyHook;
using NUnit.Framework;
using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Vanara.PInvoke;
using static Vanara.PInvoke.User32;

namespace Tests
{
    public class Tests
    {
        private const long HIWORDMASK = unchecked((long)0xffffffffffff0000L);

        private static bool IsWin32Atom(IntPtr ptr)
        {
            long lPtr = (long)ptr;
            return 0 == (lPtr & HIWORDMASK);
        }
        private static string ConvertToUnicode(IntPtr sourceBuffer, int cbSourceBuffer)
        {
            if (IsWin32Atom(sourceBuffer))
            {
                throw new ArgumentException("Arg_MustBeStringPtrNotAtom");
            }

            if (sourceBuffer == IntPtr.Zero || cbSourceBuffer == 0)
            {
                return String.Empty;
            }
            return "ConvertToUnicode";
        }

        [Test]
        public void IsWin32Atom_Test()
        {
            Assert.AreEqual(false, IsWin32Atom(IntPtr.Zero));
        }
        [Test]
        public void ConvertToUnicode_Test()
        {
            Assert.AreEqual(string.Empty, ConvertToUnicode(IntPtr.Zero, 0));
        }

        [UnmanagedFunctionPointer(CallingConvention.StdCall, CharSet = CharSet.Ansi, SetLastError = true)]
        delegate MB_RESULT MessageBoxDelegate(IntPtr owner, IntPtr lpText, string lpCaption, MB_FLAGS uType);


        [DllImport("user32", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, SetLastError = true)]
        static extern MB_RESULT MessageBoxA(IntPtr owner, string lpText, string lpCaption, MB_FLAGS uType);



        static MB_RESULT MyMessageBox(IntPtr owner, IntPtr lpText, string lpCaption, MB_FLAGS uType)
        {
            Console.WriteLine((lpText == IntPtr.Zero?"null" : Marshal.PtrToStringAnsi(lpText)) + $";{lpCaption??"null"}");
            return MB_RESULT.IDNO;
        }



        [Test]
        public void TestNullHook()
        {
            var hook = LocalHook.Create(
                   LocalHook.GetProcAddress("user32.dll", "MessageBoxA"),
                   new MessageBoxDelegate(MyMessageBox),
                   null);
            hook.ThreadACL.SetExclusiveACL(new int[0]);
            MessageBoxA(IntPtr.Zero, "*заголовок*","caption***", MB_FLAGS.MB_HELP);
        }
    }
}