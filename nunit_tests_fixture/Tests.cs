using NUnit.Framework;
using System;

namespace nunit_tests_fixture
{
    public class Test2
    {
        [Test]
        public void Test()
        {
            Console.WriteLine($"Test {nameof(SetUpFixtureBase.OneTimeSetUpCount)}={SetUpFixtureBase.OneTimeSetUpCount}");
        }
    }
    public class Test3
    {
        [Test]
        public void Test()
        {
            Console.WriteLine($"Test {nameof(SetUpFixtureBase.OneTimeSetUpCount)}={SetUpFixtureBase.OneTimeSetUpCount}");
        }

        [Test]
        public void ValueTupleTest()
        {
            var t1 = new ValueTuple<Int32>(100);
            var t2 = new ValueTuple<Int16>(100);

            var t3 = new ValueTuple<string, Int32>("s", 100);
            var t4= new ValueTuple<string, Int16>("s", 100);

            Assert.AreEqual((Int32)100, (Int16)100, "raw");
            Assert.AreEqual(t1, t2, "tuple12");
            Assert.AreEqual(t3, t4, "tuple34");
        }

        [TestCase("2019-10-10 11:11:11")]
        public void TestData(DateTime time)
        {
            Console.WriteLine(time);
        }

        public static TestCaseData TimeArgs => new TestCaseData("2019-10-10 11:11:11");
        [TestCaseSource(nameof(TimeArgs))]
        public void TestData2(DateTime time)
        {
            Console.WriteLine(time);
        }
    }
}