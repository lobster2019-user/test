using NUnit.Framework;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System;
using System.Reactive.Subjects;

namespace Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public async Task Observable_FromAsync_Test()
        {
            using (var ct = new System.Threading.CancellationTokenSource())
            {

                //       Observable
                //         .Range(0, 5)
                //        .Subscribe(z => System.Console.WriteLine(z));

                Observable
                    .Range(0, 5)
                    .Timestamp()
                    .Select(z =>
                    {
                        return Observable.FromAsync(async () =>
                        {
                            Console.WriteLine($"start: {z}");
                            await Task.Delay((5 - z.Value) * 1000);
                            Console.WriteLine($"end: {z}");
                            return z.Value;
                        });
                    })
                    .Merge(4)
                    //.Concat()
                    .Timestamp()
                    .Subscribe(z => System.Console.WriteLine(z), () => ct.Cancel())
                    ;
                await Task.WhenAny(Task.Delay(-1, ct.Token));
                Assert.Pass("complete");
            }
        }

        [Test]
        public void DateTimeDiff_Test()
        {
            {
                var dt_now = DateTime.Now;
                var dt_utcnow = DateTime.UtcNow;
                Console.WriteLine(string.Format("{0} - {1} = {2}", dt_now, dt_utcnow, (dt_now - dt_utcnow)));
                Assert.AreNotEqual(Math.Round((dt_now - dt_utcnow).TotalSeconds), 0, "datetime");
            }
            {
                var dt_now = DateTimeOffset.Now;
                var dt_utcnow = DateTimeOffset.UtcNow;
                Console.WriteLine(string.Format("{0} - {1} = {2}", dt_now, dt_utcnow, (dt_now - dt_utcnow).TotalSeconds));
                Assert.AreEqual(Math.Round((dt_now - dt_utcnow).TotalSeconds), 0, "datetimeoffset");
            }
        }

        [Test]
        public void Observable_Do_Where_Test()
        {
            var source = Observable.Range(1, 5);
            source
                .Do(z => Console.WriteLine($"log: {z}"))
                .Where(z => z % 2 == 0)
                .Do(z => Console.WriteLine($"where: {z}"))
                .Subscribe();
            ;
        }


        [Test]
        public async Task Observable_Subject_Test()
        {
            var sub = new Subject<Int32>();
            sub.Timestamp().Subscribe(z => Console.WriteLine(z));

            sub.OnNext(0);
            await Task.Delay(1000);
            sub.OnNext(1);
            await Task.Delay(1000);
            sub.OnNext(2);
            await Task.Delay(1000);
            sub.OnCompleted();
            await Task.Delay(5000);

        }
        [Test]
        public async Task Observable_AsyncSubject_Test()
        {
            var sub = new AsyncSubject<Int32>();
            sub.Timestamp().Subscribe(z => Console.WriteLine(z));

            sub.OnNext(0);
            await Task.Delay(1000);
            sub.OnNext(1);
            await Task.Delay(1000);
            sub.OnNext(2);
            await Task.Delay(1000);
            sub.OnCompleted();
            await Task.Delay(5000);

        }

        [Test]
        public void Observable_BehaviorSubject_Test()
        {
            var sub = new BehaviorSubject<Int32>(0);
            sub.Timestamp().Subscribe(z => Console.WriteLine(z));
            sub.OnNext(1);
            sub.Timestamp().Subscribe(z => Console.WriteLine(z));
        }


        [Test]
        public void Observable_RelaySubject_Test()
        {
            var sub = new ReplaySubject<Int32>(2);
            sub.OnNext(0);
            sub.OnNext(1);
            sub.OnNext(2);
            sub.OnNext(3);
            sub.Timestamp().Subscribe(z => Console.WriteLine(z));
            sub.OnNext(4);
            sub.Timestamp().Subscribe(z => Console.WriteLine(z));
        }

        [Test]
        public void Observable_Connect_Test()
        {
            var source = Observable.Range(1, 5);
            var connection = source.Publish();
        }
    }
}