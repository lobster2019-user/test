using NUnit.Framework;
using StreamJsonRpc;
using System;
using System.Buffers;
using System.Buffers.Text;
using System.IO.Pipelines;
using System.Net;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;

namespace Tests
{
    public partial class Tests
    {
        [SetUp]
        public void Setup()
        {
        }
        public interface IServer
        {
            Task<Int32> Add(Int32 a, Int32 b);
        }
        [Test]
        public async Task BufferTest()
        {
            var pipe = new Pipe();
            for (var i = 0; i < 100; ++i)
            {
                var m = pipe.Writer.GetMemory(100);
                m.Span.Fill((byte)(i + 1));
                pipe.Writer.Advance(100);
                await pipe.Writer.FlushAsync();
            }
            var result = await pipe.Reader.ReadAsync();
            Console.WriteLine(result.Buffer.Length);
            var start = result.Buffer.Start;
            while(result.Buffer.TryGet(ref start, out var mem))
            {
                Console.WriteLine(mem.Length);
            }
        }
        [Test]
        public async Task TestSeparatedRpcClientAsync()
        {
            using (var ws = new ClientWebSocket())
            {
                ws.Options.Proxy = new WebProxyForce(new WebProxy("http://127.0.0.1:8888/"));
                await ws.ConnectAsync(
                                 uri: new Uri("ws://localhost:39993/jsonrpc/"),
                   cancellationToken: CancellationToken.None);

                var text = "[{\"jsonrpc\":\"2.0\", \"method\":\"Add\", \"params\" : [ 4,5 ], \"id\":\"2\" },";
                var text2 = "{\"jsonrpc\":\"2.0\", \"method\":\"Add\", \"params\" : [ 2,3 ], \"id\":\"1\" }]";
                var data = (System.Text.Encoding.UTF8.GetBytes(text));
                var data2 = (System.Text.Encoding.UTF8.GetBytes(text2));
                var data3 = (System.Text.Encoding.UTF8.GetBytes(text + text2));

                var memory2 = new Memory<byte>(data2);
                var memory3 = new Memory<byte>(data3);

                var memory0 = new Memory<byte>(data, 0, 20);
                //await ws.SendAsync(memory0, WebSocketMessageType.Text, false, CancellationToken.None);

                var memory1 = new Memory<byte>(data, 20, data.Length - 20);
                //await ws.SendAsync(memory1, WebSocketMessageType.Text, false, CancellationToken.None);

                //await ws.SendAsync(memory2, WebSocketMessageType.Text, true, CancellationToken.None);

                await ws.SendAsync(memory3, WebSocketMessageType.Text, true, CancellationToken.None);

                await ws.ReceiveAsync(new Memory<byte>(new byte[1024]), new CancellationTokenSource(5000).Token);
                await ws.ReceiveAsync(new Memory<byte>(new byte[1024]), new CancellationTokenSource(5000).Token);

                await ws.CloseAsync(WebSocketCloseStatus.NormalClosure, "", CancellationToken.None);

            }
            Assert.Pass();
        }
        [Test]
        public async Task TestRpcClientAsync()
        {
            using (var ws = new ClientWebSocket())
            {
                ws.Options.Proxy = new WebProxyForce(new WebProxy("http://127.0.0.1:8888/"));
                await ws.ConnectAsync(
                                 uri: new Uri("ws://localhost:39993/jsonrpc/"),
                   cancellationToken: CancellationToken.None);

                using (var rpc = new JsonRpc(new WebSocketMessageHandler(ws)))
                {
                    var service = rpc.Attach<IServer>();
                    rpc.StartListening();
                    var addResult = await rpc.InvokeAsync<Int32>("Add", 1, 2);
                    var addResult2 = await service.Add(2, 3);
                    Assert.AreEqual(3, addResult);
                    Assert.AreEqual(5, addResult2);
                    await ws.CloseAsync(WebSocketCloseStatus.NormalClosure, "", CancellationToken.None);
                }
            }
            Assert.Pass();
        }
    }
}