using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public async Task MemoryCache_SizeLimit_CompactionPercentage()
        {
            MemoryCache cache = new MemoryCache(
               Options.Create(new MemoryCacheOptions
               {
                   SizeLimit = 1000,
                   CompactionPercentage = 0.25,
               }));
            cache.Set("12345", new string('a', 1), new MemoryCacheEntryOptions
            {
                Size = 200,
                Priority = CacheItemPriority.Low
            });
            Assert.True(cache.TryGetValue("12345", out var _), "_1");

            for (var i = 0; i < 10; ++i)
            {
                cache.Set("123456" + i, new string('a', 1), new MemoryCacheEntryOptions
                {
                    Size = 200,
                    Priority = CacheItemPriority.High,
                });
            }


            await Task.Delay(500);
            Assert.AreEqual(3, cache.Count, "count = 2");
            Assert.False(cache.TryGetValue("12345", out var _), "_2");
        }

        [Test]
        public async Task DistributedCache_CompactionPercentage()
        {

            MemoryDistributedCache cache = new MemoryDistributedCache(
           Options.Create(new MemoryDistributedCacheOptions
           {
               SizeLimit = 1000,
               CompactionPercentage = 0.25,
           }));
            await cache.SetStringAsync("first", new string('a', 201));
            Assert.NotNull(await cache.GetStringAsync("first"), "after.inserted");

            for (var i = 0; i < 4; ++i)
            {           
                await cache.SetStringAsync($"second[{i}]", new string('a', 200));
            }
            await Task.Delay(15);
            Assert.IsNull(await cache.GetStringAsync("first"), "after.new.items.inserted");

            //await Task.Delay(5000);
            //Assert.AreEqual(3, cache., "count = 2");
            //Assert.IsNull(await cache.GetStringAsync("first"), "after.timeout");
        }
    }
}