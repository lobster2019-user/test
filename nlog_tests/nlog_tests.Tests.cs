
using NLog;
using NLog.Config;
using NLog.LayoutRenderers;
using NLog.LayoutRenderers.Wrappers;
using NLog.Layouts;
using NLog.MessageTemplates;
using NLog.Targets;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Tests
{
    public class Tests
    {
        /*
        //   [Test]
        public void Test2()
        {
            //SimpleLayout l = (@"${uppercase:${message} ${onexception:EXCEPTION1\\: ${exception:format=message}} ${logger}}");
            SimpleLayout l = (@"${message} ${onexception:EXCEPTION1\: ${exception:format=message}} ${logger}");

            var le = LogEventInfo.Create(LogLevel.Info, "logger", "message");
            //Assert.AreEqual("messagelogger", l.Render(le));

            var le2 = LogEventInfo.Create(LogLevel.Info, "logger", "message");
            le2.Exception = new InvalidOperationException("Exception2Message");

            Assert.AreEqual("message EXCEPTION1: Exception2Message logger", l.Render(le2));
        }

        /// <summary>
        /// https://github.com/NLog/NLog/issues/3193
        /// </summary>
     //   [Test]
        public void Nlog_issue_3193_ParseParameterName()
        {
            var input = @"${var:default-path}/${cached:${appdomain:format={1\} {0\}}}.${cached:${date:format=yyyyMMdd}}.info.log";
            var reader = new NLog.Internal.SimpleStringReader(input);
            reader.Position = 29;
            var res1 = LayoutParser.ParseParameterName(reader);
            Assert.AreEqual(@"${appdomain:format={1\} {0\}}", res1);
        }
        /// <summary>
        /// https://github.com/NLog/NLog/issues/3193
        /// </summary>
   //     [Test]
        public void Nlog_issue_3193_ParseParameterName_2()
        {
            var input = @"${var:default-path}/${cached:${appdomain:format={1\} {0\}}}.${cached:${date:format=yyyyMMdd}}.info.log";
            var reader = new NLog.Internal.SimpleStringReader(input);
            reader.Position = 29;
            var res1 = LayoutParser.ParseParameterName(reader);
            Assert.AreEqual(@"${appdomain:format={1\} {0\}}", res1);
        }

        //     [Property("Issue", "3193")]
        //     [TestCase(@"                                    ${literal:text={0\} {1\}}")]
        //     [TestCase(@"                           ${cached:${literal:text={0\} {1\}}}")]
        //      [TestCase(@"                  ${cached:${cached:${literal:text={0\} {1\}}}}")]
        //     [TestCase(@"         ${cached:${cached:${cached:${literal:text={0\} {1\}}}}}")]
        //        [TestCase(@"${cached:${cached:${cached:${cached:${literal:text={0\} {1\}}}}}}")]
        public void Issue_3193_Nested_�losing_Braces(string input)
        {
            SimpleLayout simple = input.Trim();
            var result = simple.Render(new LogEventInfo { });
            Assert.AreEqual("{0} {1}", result);
        }

        //     [SetUp]
        public void Setup()
        {
        }

        //     [Test]
        public void Test1()
        {
            System.Environment.SetEnvironmentVariable("LOGDIR", null);
            Assert.AreEqual(null, System.Environment.GetEnvironmentVariable("LOGDIR"));
            NLog.LogManager.Configuration = new NLog.Config.XmlLoggingConfiguration("nlog.config");
            var logger = NLog.LogManager.GetCurrentClassLogger();
            //logger.Info(System.IO.File.ReadAllText("nlog.config"));
            for (var i = 0; i < 1; ++i)
            {
                logger.Info("hello*");
                logger.Error("error*");
                //Thread.Sleep(10000);
            }
            //System.Environment.UserDomainName
            NLog.LogManager.Shutdown();
            Assert.Pass();
        }

        [Test]
        public void shutdown()
        {
            Console.WriteLine(typeof(LogFactory));
            Console.WriteLine(typeof(LogManager));
            var logger = LogManager.GetCurrentClassLogger();

            var context = System.AppDomain.CurrentDomain;

            var field = typeof(AppDomain).GetEvent("ProcessExit",
                    System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            //var processExit = field.get(context);
            //Console.WriteLine(processExit);
        }
        */
        [OneTimeSetUp]
        public void InitializeLog()
        {
            var config = new LoggingConfiguration();
            LayoutRenderer.Register("indent", typeof(ScopedLayoutRenderer));
            config.AddRuleForAllLevels(new ColoredConsoleTarget()
            {
                Layout = new SimpleLayout(@"${time} ${logger} ${indent} ${message} ${exception:format=toString,data:exceptionDataSeparator=\r\n}")
            });
            LogManager.Configuration = config;
        }

        [Test]
        public void TestAssemblyAndModule()
        {
            foreach (var assembly in System.AppDomain.CurrentDomain.GetAssemblies())
            {
                Console.WriteLine($"assembly: {assembly.FullName}");
                var i = 0;
                foreach (var module in assembly.Modules)
                {
                    Console.WriteLine($"\t[{i}]module: {module.FullyQualifiedName}");
                    ++i;
                }
            }
        }
        [Test]
        [LogInterceptorAttribute]
        public void TestMethod2Decorator()
        {
            Console.WriteLine("sync");
        }
        [Test]
        [LogInterceptorAttribute]
        public async Task TestMethodDecoratorAsync()
        {
            LogManager.GetCurrentClassLogger().Trace("1");
            await Task.Delay(500);
            LogManager.GetCurrentClassLogger().Trace("2");
            await Task.Delay(500);
            LogManager.GetCurrentClassLogger().Trace("3");
            //throw new Exception();
        }
    }
}