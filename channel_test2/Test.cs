﻿using NUnit.Framework;
using System;
using System.Threading.Channels;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace channel_test2
{
    public class Test
    {
        public const Int32 Capacity = 3;

        private void TryFillAssert(ChannelWriter<Int32> writer, Int32 capacity)
        {
            for (var i = 0; i < capacity; ++i)
            {
                Assert.True(writer.TryWrite(i));
            }
        }
        private Channel<Int32> CreateBounded(BoundedChannelFullMode mode)
        {
            return Channel.CreateBounded<Int32>(
                new BoundedChannelOptions(Capacity)
                {
                    SingleReader = true,
                    SingleWriter = true,
                    FullMode = mode,
                    AllowSynchronousContinuations = true,
                });
        }
        private Channel<Int32> CreateUnbounded()
        {
            return Channel.CreateUnbounded<Int32>(
                new UnboundedChannelOptions()
                {
                    SingleReader = true,
                    SingleWriter = true,
                    AllowSynchronousContinuations = true,
                });
        }

        [TestCase(Capacity, BoundedChannelFullMode.DropWrite, true)]
        [TestCase(Capacity, BoundedChannelFullMode.DropNewest, true)]
        [TestCase(Capacity, BoundedChannelFullMode.DropOldest, true)]
        [TestCase(Capacity, BoundedChannelFullMode.Wait, false)]
        public void Channel_TryWriteCapacityPlusOne(Int32 capacity, BoundedChannelFullMode mode, bool result)
        {
            var channel = CreateBounded(mode);
            TryFillAssert(channel.Writer, capacity);
            Assert.AreEqual(result, channel.Writer.TryWrite(capacity));
        }


        [TestCase(Capacity)]
        public void Channel_FullModeWait_WriteAsync_False(Int32 capacity)
        {
            var channel = Channel.CreateBounded<Int32>(
                new BoundedChannelOptions(capacity)
                {
                    SingleReader = true,
                    SingleWriter = true,
                    FullMode = BoundedChannelFullMode.Wait,
                });
            TryFillAssert(channel.Writer, capacity);

        }

        [TestCase(Capacity)]
        public void DataFlow_FullModeWait_WriteAsync_False(Int32 capacity)
        {
            var buffer = new BufferBlock<Int32>(new DataflowBlockOptions
            {
                 BoundedCapacity = 3,
            });
            for(var i = 0; i < capacity; ++i)
            {
                Assert.True(buffer.Post(i));
            }
            Assert.False(buffer.Post(capacity));

            var batch
                 = new BatchBlock<int>(2, new GroupingDataflowBlockOptions
                 {
                      BoundedCapacity=3,
                       
                 }
                  );
            batch.TriggerBatch();
            
        }
    }
}
