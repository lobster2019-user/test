﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace signalr__server
{
    public class JsonRpcConnectionHandlerExtensions
    {
        public static IServiceCollection AddSignalRCore(IServiceCollection services)
        {
            services.TryAddSingleton(typeof(HubConnectionHandler<>), typeof(JsonRpcConnectionHandler<>));
            return services;
        }
    }
}
