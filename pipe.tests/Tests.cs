using NUnit.Framework;
using System;
using System.IO.Pipelines;
using System.Threading;
using System.Threading.Tasks;

namespace Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }
        Int64 GetLength(Pipe pipe)
        {
            return (Int64)pipe.GetType().GetProperty("Length", System.Reflection.BindingFlags.NonPublic |
                System.Reflection.BindingFlags.Instance).GetValue(pipe);
        }
        //16*2048
        [Test]
        public async Task Test_Threshold()
        {
            var total = 0;
            var ts = new CancellationTokenSource();
            ts.CancelAfter(5000);
            var token = ts.Token;
            var pipe = new Pipe();
            for(var i = 0; i < 20; ++i)
            {
                pipe.Writer.GetSpan(2000).Fill(1);
                pipe.Writer.Advance(2000);
                Console.WriteLine("flushing 2000");                
                await pipe.Writer.FlushAsync(token);
                total += 2000;
                Console.WriteLine($"flushed {total}");
            }
        }
        [Test]
        public async Task Test1()
        {
            var pipe = new Pipe();
            pipe.Writer.GetSpan(100).Fill(1);
            pipe.Writer.Advance(99);
            await pipe.Writer.FlushAsync();
            Assert.AreEqual(99, GetLength(pipe));
            //Assert.Throws<InvalidOperationException>(() => pipe.Writer.Advance(20));

            pipe.Writer.GetSpan(1).Fill(2);
            pipe.Writer.Advance(1);
            await pipe.Writer.FlushAsync();
            Assert.AreEqual(100, GetLength(pipe));

            //Assert.Throws<InvalidOperationException>(() => pipe.Writer.Advance(20));
            ReadResult readBuffer;

            readBuffer = await pipe.Reader.ReadAsync();
            pipe.Reader.AdvanceTo(
                readBuffer.Buffer.GetPosition(3, readBuffer.Buffer.Start),
                readBuffer.Buffer.GetPosition(0, readBuffer.Buffer.End));



            Assert.AreEqual(100 - 3, GetLength(pipe));

            pipe.Writer.GetSpan(11).Fill(3);
            pipe.Writer.Advance(11);
            await pipe.Writer.FlushAsync();

            Assert.AreEqual(100 - 3 + 11, GetLength(pipe));

            readBuffer = await pipe.Reader.ReadAsync();
            pipe.Reader.AdvanceTo(readBuffer.Buffer.End);

            Assert.AreEqual(0, GetLength(pipe));

            pipe.Writer.Complete();
            readBuffer = await pipe.Reader.ReadAsync();
            Assert.IsTrue(readBuffer.IsCompleted);
        }
    }
}