﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace nunit_tests_fixture
{
    [NUnit.Framework.SetUpFixture]
    public class SetUpFixtureBase
    {
        public static Int32 OneTimeSetUpCount { get; set; }

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ++OneTimeSetUpCount;
            Console.WriteLine("Console: {0} {1}", this.GetType().Name, "OneTimeSetUp");
            Debug.WriteLine(string.Format("Debug: {0} {1}", this.GetType().Name, "OneTimeSetUp"));
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            Console.WriteLine("{0} {1}", this.GetType().Name, "OneTimeTearDown");
        }
    }
}
