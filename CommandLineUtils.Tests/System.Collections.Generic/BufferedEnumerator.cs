﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using global::System;

namespace Lobster.System.Collections.Generic
{
    public class BufferedEnumerator<T> : IBufferedEnumerator<T>
    {
        private readonly IEnumerator<T> _enumerator;
        private readonly bool _canGoBackward;
        private readonly bool _canGoForward;
        private readonly int _maxCacheSize;
        private Int32 _currentIndex = -1;
        private Int32 _cacheOffset = 0;
        private readonly List<T> _cache;
        private bool _disposed = false;
        private readonly T[] _tmp = new T[1];

        public BufferedEnumerator(IEnumerator<T> enumerator, bool canGoBackward, bool canGoForward, Int32 maxCacheSize)
        {
            if (maxCacheSize < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(maxCacheSize));
            }

            _enumerator = enumerator;
            _canGoBackward = canGoBackward;
            _canGoForward = canGoForward;
            _maxCacheSize = maxCacheSize;
            if (canGoBackward || canGoBackward)
            {
                _cache = new List<T>();
            }
            else
            {
                _cache = new List<T>(1);
            }
        }

        private Int32 GetCacheIndex(Int32 index) => index - _cacheOffset;

        private void CheckCacheIndex(Int32 index)
        {
            if (index < 0 || index >= _cache.Count)
            {
                throw new ArgumentOutOfRangeException(nameof(index));
            }
        }

        private T GetItemFromCache(Int32 index)
        {
            index = GetCacheIndex(index);
            CheckCacheIndex(index);
            return _cache[index];
        }

        public T this[int index]
        {
            get
            {
                CheckDisposed();
                return GetItemFromCache(index);
            }
        }

        public int Position
        {
            get
            {
                CheckDisposed();
                return _currentIndex;
            }
            set
            {
                CheckCacheIndex(GetCacheIndex(value));
                _currentIndex = value;
            }
        }

        public T Current => this[_currentIndex];

        object IEnumerator.Current => Current;

        private void CheckDisposed()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(this.GetType().Name);
            }
        }

        private void ResetImpl()
        {
            _currentIndex = -1;
            _cacheOffset = 0;
            _cache.Clear();
            _enumerator.Reset();
        }

        public void Dispose()
        {
            if (!_disposed)
            {
                _disposed = true;
                ResetImpl();
                _enumerator.Dispose();
            }
        }

        private Int32 PrepareCacheForNewItems(Int32 newItemsCount)
        {
            if (newItemsCount < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(newItemsCount));
            }
            Int32 removedFromCache = 0;
            if (_maxCacheSize > 0)
            {
                if (newItemsCount > _maxCacheSize)
                {
                    throw new ArgumentOutOfRangeException(nameof(newItemsCount));
                }
                removedFromCache = _cache.Count + newItemsCount - _maxCacheSize;
                if(_cacheOffset + removedFromCache > _currentIndex)
                {
                    throw new ArgumentOutOfRangeException(nameof(newItemsCount));
                }
                for (var i = 0; i < removedFromCache; ++i)
                {
                    _cache.RemoveAt(0);
                    ++_cacheOffset;
                }
            }
            return removedFromCache;
        }

        public bool MoveNext()
        {
            CheckDisposed();

            // Next item does not exist
            var index = GetCacheIndex(_currentIndex);
            if (index + 1 >= 0 && index + 1 < _cache.Count - 1)
            {
                ++_currentIndex;
                return true;
            }

            if (!_enumerator.MoveNext())
            {
                return false;
            }
            if (!_canGoBackward)
            {
                //cache is empty or has one item = current
                if (_currentIndex >= 0)
                {
                    Debug.Assert(0 == GetCacheIndex(_currentIndex));
                    _cache.RemoveAt(0);
                    ++_cacheOffset;
                }
            }
            else
            {
                PrepareCacheForNewItems(1);
            }
            ++_currentIndex;
            Debug.Assert(_cache.Count == _currentIndex);
            _cache.Add(_enumerator.Current);

            return true;
        }

        public void Reset()
        {
            CheckDisposed();
            ResetImpl();
        }

        public bool TryPeek(out T result)
        {
            CheckDisposed();
            if(1 == TryPeek(1, 1, _tmp, 0))
            {
                result = _tmp[0];
                _tmp[0] = default(T);
                return true;
            }
            result = default(T);
            return false;
        }

        public int TryPeek(int srcOffset, int count, T[] dst, int dstOffset)
        {
            CheckDisposed();

            if (!_canGoForward)
            {
                throw new InvalidOperationException();
            }

            if(dst.Length < dstOffset + count)
            {
                throw new ArgumentOutOfRangeException("dst.Length < dstOffset + count");
            }
            var position = GetCacheIndex(_currentIndex + srcOffset);
            if(position < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(srcOffset));
            }
            if(_maxCacheSize > 0)
            {
                if(position + count - GetCacheIndex(_currentIndex) > _maxCacheSize)
                {
                    throw new ArgumentOutOfRangeException("position + count - GetCacheIndex(_currentIndex) > _maxCacheSize");
                }
            }
            Int32 peeked = 0;


            while (count > 0 && position < _cache.Count )
            {
                dst[dstOffset + peeked] = _cache[position];
                --count;
                ++position;
                ++peeked;
            }

            if (count > 0)
            {
                Debug.Assert(position >= _cache.Count);
                while(position > _cache.Count)
                {
                    if (_enumerator.MoveNext())
                    {
                        PrepareCacheForNewItems(1);
                        _cache.Add(_enumerator.Current);
                    }
                    else
                    {
                        return peeked;
                    }
                }
                while(count > 0)
                {
                    if (_enumerator.MoveNext())
                    {
                        PrepareCacheForNewItems(1);
                        _cache.Add(dst[dstOffset + peeked] = _enumerator.Current);
                        ++peeked;
                        --count;
                    }
                    else
                    {
                        return peeked;
                    }
                }
            }
            return peeked;
        }
    }
}