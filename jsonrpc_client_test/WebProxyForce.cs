﻿using System;
using System.Net;

namespace Tests
{
    public class WebProxyForce : IWebProxy
    {
        private readonly WebProxy _proxy;

        public WebProxyForce(WebProxy proxy)
        {
            _proxy = proxy;
        }

        public ICredentials Credentials { get => _proxy.Credentials; set => this._proxy.Credentials = value; }

        public Uri GetProxy(Uri destination) => _proxy.Address;

        public bool IsBypassed(Uri host) => false;
    }

}