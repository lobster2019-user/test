﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace udp_client
{
    public class MemoryHttpMessageHandler : System.Net.Http.HttpMessageHandler
    {
        protected override Task<HttpResponseMessage> 
            SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
    class Program
    {
        static async Task Main(string[] args)
        {
            Refit.RestService.For<object>(client: null);

        }
        static async Task Main2(string[] args)
        {

            UdpClient server = new UdpClient(8001);
            IPEndPoint ip = null;

            UdpClient client = new UdpClient();
            string message = "Hello world!";
            byte[] data = Encoding.UTF8.GetBytes(message);
            await client.SendAsync(data, data.Length, "127.0.0.1", 8001);

            var result = await server.ReceiveAsync();
            ip = result.RemoteEndPoint;
            data = result.Buffer;

            message = Encoding.UTF8.GetString(data);

            Console.WriteLine("received: {0}", message);

            server.Close();

            client.Close();

        }
    }
}
