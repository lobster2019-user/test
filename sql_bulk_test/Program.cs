﻿using System;
using System.Data.SqlClient;
using System.Diagnostics;

namespace sql_bulk_test
{
    partial class Program
    {
        static void ShowMemory()
        {
            Console.WriteLine($"{System.Environment.WorkingSet / 1024.0 / 1024.0} mb");
        }
        static void Main(string[] args)
        {
            using (var sql = new SqlConnection(
                @"Server=127.0.0.1;Database=testdb;Trusted_Connection=True"))
            {
                sql.Open();
                ShowMemory();

                using (var cmd = new SqlCommand())
                {
                    cmd.Connection = sql;
                    cmd.CommandText = "truncate table [messages]";
                    cmd.ExecuteNonQuery();
                }
                /*
                var dt = new DataTable();
                dt.Columns.Add(new DataColumn { ColumnName = "Message", DataType = typeof(String) });
                for (var i = 0; i < 100000; ++i)
                {
                    var row = dt.NewRow();
                    row[0] = i.ToString();
                    dt.Rows.Add(row);
                }
                Console.WriteLine("created datatable");*/
                ShowMemory();
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                Console.WriteLine("start");
                using (var bulk = new SqlBulkCopy(sql))
                {
                    bulk.ColumnMappings.Add(0, "Message");
                    bulk.BatchSize = 1000;
                    bulk.EnableStreaming = true;
                    bulk.DestinationTableName = "messages";
                    bulk.BulkCopyTimeout = 600;
                    //bulk.SqlRowsCopied += Bulk_SqlRowsCopied;
                    //bulk.WriteToServer(dt);
                    bulk.WriteToServer(new MyReader(100000));                    
                }
                stopwatch.Stop();
                Console.WriteLine($"wrote to server {stopwatch.ElapsedMilliseconds} ms");
                ShowMemory();
                Console.WriteLine("Hello World!");
            }
        }

        private static void Bulk_SqlRowsCopied(object sender, SqlRowsCopiedEventArgs e)
        {
            Console.WriteLine(e.RowsCopied);
        }
    }
}
