﻿using FluentValidation;
using System;
using System.Diagnostics;

namespace Tests
{
    public static class AbstractValidatorExtensions
    {
        public static IConditionBuilder If<T>(
            this AbstractValidator<T> validator,
            Func<T, bool> predicate,
            Action action)
        {
            return validator.When(predicate, action);
        }
    }
    public class AbcValidator2 : AbstractValidator<ABC>
    {
        public AbcValidator2()
        {
            RuleFor(t => t)
                .Must(t => t.A > 100).WithMessage("condition: A>100")
                .Must(t => t.B > 100).WithMessage("condition: B>100").WithSeverity(Severity.Warning)
                .When(t => t.C > 99);
        }
    }
    public class AbcValidator : AbstractValidator<ABC>
    {
        public AbcValidator()
        {


            When(z => z.A > 0, () =>
                 {
                     Debug.WriteLine("z.A > 0");
                     this.RuleFor(t => t)
                        .Must(t => t.B > 0);
                 })
                 .Otherwise(() =>
                 {
                     Debug.WriteLine("z.A <= 0");
                     this.RuleFor(t => t)
                        .Must(t => t.B <= 0);
                 });
        }
    }
}