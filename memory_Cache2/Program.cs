﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using System;

namespace memory_Cache2
{
    class Program
    {
        static void Main(string[] args)
        {
            IMemoryCache cache = new MemoryCache(
                Options.Create(new MemoryCacheOptions
                {
                    SizeLimit = 1000,
                }));
            Console.WriteLine("Hello World!");
        }
    }
}
