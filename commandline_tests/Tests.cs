using CommandLine;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Tests
{
    public class Poco2DataOptions
    {
        [Option('n', "navigation", HelpText = "Add navigation properties")]
        public bool Navigation { get; set; }
    }
    public class Options_358
    {
        [Option('n', "name", Required = false)]
        public string Name { get; set; }
    }
    public class Options_412
    {
        [Option('d', "description", HelpText = "Set the description from the project")]
        public string Description { get; set; }
    }
    public class Options_420
    {
        [Option('b', "BookIds",
              Required = false,
              Default = new string[] { },
              HelpText = "List of Books",
              Separator = ',')]
        public IEnumerable<string> Bookids { get; set; }

        [Option('v', "Verbose",
            Required = false,
            Hidden = true,
            HelpText = "Generate Book details",
            Default = "Yes")]
        public string All { get; set; }
    }

    public class Tests
    {
        
        [SetUp]
        public void Setup()
        {
        }
        [TestCase(arguments: new object[] { "issue_358", typeof(Options_358), new[] { "-n ", "" } })]
        [TestCase(arguments: new object[] { "issue_412_1", typeof(Options_412), new[] { "--description","" } })]
        [TestCase(arguments: new object[] { "issue_412_2", typeof(Options_412), new[] { "--description=" } })]
        [TestCase(arguments: new object[] { "issue_420", typeof(Options_420), new[] { "-b -v NO" } })]
        [TestCase(arguments: new object[] { "passed", typeof(Poco2DataOptions), new[] { "-n" } })]
        public void TestArgs(string issue, Type options, string[] args)
        {
            typeof(Tests).GetMethod(
                nameof(TestArgsT))
                .MakeGenericMethod(options).Invoke(this, new object[] { issue, args });
        }

        public void TestArgsT<T>(string issue, string[] args)
        {
            var result = Parser.Default.ParseArguments<T>(args)
                    .WithParsed(o =>
                    {
                        //Assert.Pass($"{issue}: Parsed");
                    })
                    .WithNotParsed(errors =>
                    {
                        Assert.Fail($"{issue}: {string.Join("\r\n", errors.Select(error => error.Tag))}");
                    });
            
        }
    }
}