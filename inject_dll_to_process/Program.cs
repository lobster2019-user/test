﻿using EasyHook;
using System;

namespace inject_dll_to_process
{
    class Program
    {
        static void Main(string[] args)
        {
            
            var location = typeof(InjectedClass).Assembly.Location;
            RemoteHooking.Inject(
                      InTargetPID: System.Diagnostics.Process.GetProcessesByName("dnspy")[0].Id,
                InLibraryPath_x86: location,
                InLibraryPath_x64: location);
            Console.Read();
        }
       
    }
}
