using NUnit.Framework;
using System.Collections.Generic;

namespace TestsNS
{
    public class TestsClass
    {
        [SetUp]
        public void Setup()
        {
        }
        [TearDown]
        public void TearDown()
        {

        }


        public static string[] Data => new string[] { "a", "b", "c" };
        public static IEnumerable<TestCaseData> Data2() 
        {
            yield return new TestCaseData("d");
        }

        [NUnit.Framework.TestCaseSource(typeof(TestsClass), nameof(Data))]
        public void TestMethodName(string p)
        {
            Assert.AreEqual(1, p.Length);
        }
        [NUnit.Framework.TestCaseSource(typeof(TestsClass), nameof(Data2))]
        public void TestMethodName2(string p)
        {
            Assert.AreEqual(1, p.Length);
        }
    }
}