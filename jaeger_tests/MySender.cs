﻿using Jaeger;
using System.Threading.Tasks;
using System.Threading;
using Jaeger.Senders;
using Microsoft.Extensions.Logging;

namespace Tests
{
    public class MySender :ISender
    {
        private readonly ISender _sender;
        private readonly ILogger _logger;

        public MySender(ISender sender, ILogger logger)
        {
            _sender = sender;
            _logger = logger;
        }

        public Task<int> AppendAsync(Span span, CancellationToken cancellationToken)
        {
            _logger.LogTrace("AppendAsync");
            return _sender.AppendAsync(span, cancellationToken);
        }

        public Task<int> CloseAsync(CancellationToken cancellationToken)
        {
            _logger.LogTrace("closeasync");
            return _sender.CloseAsync(cancellationToken);
        }

        public Task<int> FlushAsync(CancellationToken cancellationToken)
        {
            _logger.LogTrace("flushasync");
            return _sender.FlushAsync(cancellationToken);
        }
    }
}