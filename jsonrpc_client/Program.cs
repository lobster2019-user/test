﻿using System;

namespace jsonrpc_client
{
    class Program
    {
        static void Main(string[] args)
        {
            new Tests.Tests().TestRpcClientAsync().Wait();
        }
    }
}
