using Lobster.System.Collections.Generic;
using McMaster.Extensions.CommandLineUtils;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Tests
{

    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            Assert.AreEqual(0, CommandLineApplication.Execute<App>
                (new[] { "-s", "sss", "-s2", "val1", "val2", "-s3", "val3" }));
        }

        [Test]
        public void PeekableEnumeratorDisposedTest()
        {
            using (var enumerator = Array.Empty<Int32>().AsPeekable().GetEnumerator())
            {
                enumerator.Dispose();

                Assert.Throws<ObjectDisposedException>(() => { var _ = enumerator.Current; });
                Assert.Throws<ObjectDisposedException>(() => { var _ = enumerator.Peek; });
                Assert.Throws<ObjectDisposedException>(() => { var _ = enumerator.CanPeek; });
                Assert.Throws<ObjectDisposedException>(() => { enumerator.MoveNext(); });
                Assert.Throws<ObjectDisposedException>(() => { enumerator.Reset(); });
            }
        }
        [Test]
        public void PeekableEnumeratorTest()
        {
            using (var enumerator = new[] { 1, 2, 3 }.AsPeekable().GetEnumerator())
            {
                for (var i = 0; i < 2; ++i)
                {
                    Assert.Throws<InvalidOperationException>(() => { var _ = enumerator.Current; });

                    Assert.AreEqual(1, enumerator.Peek);
                    Assert.Throws<InvalidOperationException>(() => { var _ = enumerator.Current; });

                    Assert.AreEqual(1, enumerator.Peek);
                    Assert.True(enumerator.CanPeek);
                    Assert.True(enumerator.MoveNext());
                    Assert.AreEqual(1, enumerator.Current);

                    Assert.AreEqual(2, enumerator.Peek);
                    Assert.True(enumerator.CanPeek);
                    Assert.True(enumerator.MoveNext());
                    Assert.AreEqual(2, enumerator.Current);

                    Assert.AreEqual(3, enumerator.Peek);
                    Assert.True(enumerator.CanPeek);
                    Assert.True(enumerator.MoveNext());
                    Assert.AreEqual(3, enumerator.Current);

                    Assert.False(enumerator.CanPeek);
                    Assert.False(enumerator.MoveNext());
                    Assert.Throws<InvalidOperationException>(() => { var _ = enumerator.Current; });
                    Assert.Throws<InvalidOperationException>(() => { var _ = enumerator.Peek; });

                    enumerator.Reset();
                }
            }
        }

        [Test]
        public void BufferedEnumerableTest()
        {
            using (var enumerator = new[] { 1, 2, 3, 4, 5 }.AsBuffered(maxCacheSize: 4).GetEnumerator())
            {
                Int32 peeked;
                Int32[] peekedArray = new int[10];

                Assert.AreEqual(-1, enumerator.Position);

                Assert.True(enumerator.TryPeek(out peeked));
                Assert.AreEqual(1, peeked);

                Assert.True(enumerator.TryPeek(out peeked));
                Assert.AreEqual(1, peeked);

                Assert.AreEqual(3, enumerator.TryPeek(srcOffset: 1, count: 3, dst: peekedArray, dstOffset: 0));
                Assert.True(new[] { 1, 2, 3 }.SequenceEqual(peekedArray.Take(3)));

                Assert.True(enumerator.MoveNext());
                Assert.AreEqual(1, enumerator.Current);
                Assert.AreEqual(0, enumerator.Position);

                Assert.AreEqual(3, enumerator.TryPeek(srcOffset: 1, count: 3, dst: peekedArray, dstOffset: 0));
                Assert.True(new[] { 2, 3, 4 }.SequenceEqual(peekedArray.Take(3)));

                Assert.True(enumerator.MoveNext());
                Assert.AreEqual(2, enumerator.Current);
                Assert.AreEqual(1, enumerator.Position);

                Assert.True(enumerator.MoveNext());
                Assert.AreEqual(3, enumerator.Current);
                Assert.AreEqual(2, enumerator.Position);

                Assert.AreEqual(2, enumerator.TryPeek(srcOffset: 1, count: 3, dst: peekedArray, dstOffset: 0));
                Assert.True(new[] { 4 , 5}.SequenceEqual(peekedArray.Take(2)));
            }
        }
    }
}