﻿@ahmettahasakar 

I think that the `dispose` call should not help, the default is a lifetime of 2 minutes, this can be fixed.
for example
```csharp
public static IHttpClientBuilder SetHandlerLifetime(this IHttpClientBuilder builder, TimeSpan handlerLifetime)
```

```csharp
public HttpClient CreateClient(string name)
        {
            ...
            var handler = CreateHandler(name);
            var client = new HttpClient(handler, disposeHandler: false);

            var options = _optionsMonitor.Get(name);
            for (var i = 0; i < options.HttpClientActions.Count; i++)
            {
                options.HttpClientActions[i](client);
            }

            ....
        }



internal class LifetimeTrackingHttpMessageHandler : DelegatingHandler
    {
        public LifetimeTrackingHttpMessageHandler(HttpMessageHandler innerHandler)
            : base(innerHandler)
        {
        }

        protected override void Dispose(bool disposing)
        {
            // The lifetime of this is tracked separately by ActiveHandlerTrackingEntry
        }
    }
```
What I do not like in HttpConnectionPool, there are no settings such as the minimum and ~~maximum~~(```SocketsHttpHandler.MaxConnectionsPerServer```) size, and some other settings typical of the pool of connections to the database For example, I implemented my constraint with a semaphore(for GLOBAL constraint)

If support for a permanent connection is not needed at all, then keep-alive: close would be suitable for a regular pool, for the factory I don’t know.



What are your expectations from pooling?