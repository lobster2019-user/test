﻿using OData2Poco.Api;
using System;
using System.Threading.Tasks;

namespace OData2Poco.CommandLine.Custom
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var o2p = new O2P(new PocoSetting
            {

            });
            var code = await o2p.GenerateAsync(new OdataConnectionString
            {

            });
        }
    }
}
