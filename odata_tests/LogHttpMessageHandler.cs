﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Tests
{
    public class LogHttpMessageHandler : DelegatingHandler
    {

        public LogHttpMessageHandler(HttpMessageHandler target) : base(target)
        {
            
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            Console.WriteLine(request.ToString());
            return base.SendAsync(request, cancellationToken);
        }
    }
}