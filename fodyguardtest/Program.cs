﻿using NullGuard;
using System;

namespace fodyguardtest
{
    [NullGuard(ValidationFlags.All)]
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
        static void Test(string arg)
        {
            Console.WriteLine(arg);
        }
    }
}
