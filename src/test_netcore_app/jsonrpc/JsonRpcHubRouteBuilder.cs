﻿using System;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.Http.Connections;
using Microsoft.AspNetCore.Authorization;
using System.Reflection;
using Microsoft.AspNetCore.Http;

namespace test_netcore_app
{/*
    public class JsonRpcHubRouteBuilder
    {
        private readonly ConnectionsRouteBuilder _routes;

        public JsonRpcHubRouteBuilder(ConnectionsRouteBuilder routes)
        {
            _routes = routes;
        }

        public void MapHub<THub>(PathString path) where THub : Hub
        {
            MapHub<THub>(path, configureOptions: null);
        }

        public void MapHub<THub>(string path, Action<HttpConnectionDispatcherOptions> configureOptions)
            where THub : Hub
        {
            var authorizeAttributes = typeof(THub).GetCustomAttributes<AuthorizeAttribute>(inherit: true);
            var options = new HttpConnectionDispatcherOptions();
            foreach (var attribute in authorizeAttributes)
            {
                options.AuthorizationData.Add(attribute);
            }
            configureOptions?.Invoke(options);

            _routes.MapConnections(path, options, builder =>
            {
                builder.UseJsonRpcHub<THub>();
            });
        }
    }*/
}
