﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lobster.System.Collections.Generic
{
    public class BufferedEnumerable<T> : IBufferedEnumerable<T>
    {
        private readonly IEnumerable<T> _enumerable;
        private readonly bool _canGoBackward;
        private readonly bool _canGoForward;
        private readonly int _maxCacheSize;

        public BufferedEnumerable(IEnumerable<T> enumerable,
            bool canGoBackward,bool canGoForward, Int32 maxCacheSize)
        {
            if(maxCacheSize< 0)
            {
                throw new ArgumentOutOfRangeException(nameof(maxCacheSize));
            }

            _enumerable = enumerable;
            _canGoBackward = canGoBackward;
            _canGoForward = canGoForward;
            _maxCacheSize = maxCacheSize;
        }
        public IBufferedEnumerator<T> GetEnumerator()
        {
            return new BufferedEnumerator<T>(_enumerable.GetEnumerator(),
                canGoBackward: _canGoBackward, canGoForward: _canGoForward,
                maxCacheSize: _maxCacheSize);
        }
    }
}
