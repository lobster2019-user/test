﻿using System;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using System.Diagnostics;

namespace test_netcore_app
{
    public class HelloHub : Hub<IHelloClient1>
    {
        private ILogger _logger;
        public HelloHub(ILogger<HelloHub> logger,IHubContext<HelloHub> context)
        {
            Console.WriteLine("***hellohub_ctor");
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }
        public override Task OnConnectedAsync()
        {
            Console.WriteLine("***OnConnectedAsync");
            return base.OnConnectedAsync();
        }
        public override Task OnDisconnectedAsync(Exception exception)
        {
            Console.WriteLine("***OnDisconnectedAsync");
            return base.OnDisconnectedAsync(exception);
        }

        public async ValueTask<string> Invoke(string text)
        {
            _logger.LogTrace("hello invoked: {0}", text);
            await this.Clients.Caller.OnMessage(text);
            return DateTime.Now.ToString();
        }
        Stopwatch _t = new Stopwatch();
        public async Task Invoke2(string text)
        {
            var result = System.Threading.Interlocked.Increment(ref _counter);
            if (result == 3)
            {
                _t.Start();
            }
            if (result == 100000 * 5 * 3)
            {
                _t.Stop();
                Console.WriteLine("{0} seconds", (double)result * 1000 / _t.ElapsedMilliseconds );
            }
            //Console.WriteLine("{0} {1} seconds",result, _t.ElapsedMilliseconds / (double)result);
            _logger.LogTrace("hello invoked2: {0}", text);
            //await this.Clients.Caller.Client.OnMessage(text);
            return;
        }
        static volatile Int32 _counter = 0;
    }
}
