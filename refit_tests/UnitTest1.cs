using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using NUnit.Framework;
using Refit;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{

    public class Result<T>
    {
        public T Data { get; set; }
    }
    public class UserEntity
    {
        //"data": {
        public Int32 id { get; set; }//: 2,
        public string FirstName { get; set; }//": "Janet",
        public string LastName { get; set; }//: "Weaver",
        public string Avatar { get; set; }//": "https://s3.amazonaws.com/uifaces/faces/twitter/josephstein/128.jpg"
    }
    public interface ReqApi
    {
        [Get("/users/{id}")]
        Task<Result<UserEntity>> GetUserAsync(Int32 id);
    }

    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public async Task Refit_Test()
        {
            var api = RestService.For<ReqApi>("https://reqres.in/api/",
               new RefitSettings
               {
                   ContentSerializer = new JsonContentSerializer(new JsonSerializerSettings
                   {

                       ContractResolver = new DefaultContractResolver
                       {
                           NamingStrategy = new SnakeCaseNamingStrategy()
                       }
                   })
               });
            var user = await api.GetUserAsync(2);
            Assert.AreEqual(2, user.Data.id);
            Assert.Pass();
        }
    }
    /*
    public class DeliminatorSeparatedPropertyNamesContractResolver : DefaultContractResolver
    {
        readonly string separator;

        protected DeliminatorSeparatedPropertyNamesContractResolver(char separator)
        {
            this.separator = separator.ToString(CultureInfo.InvariantCulture);
        }

        protected override string ResolvePropertyName(string propertyName)
        {
            var parts = new List<string>();
            var currentWord = new StringBuilder();

            foreach (var c in propertyName.ToCharArray())
            {
                if (Char.IsUpper(c) && currentWord.Length > 0)
                {
                    parts.Add(currentWord.ToString());
                    currentWord.Clear();
                }

                currentWord.Append(char.ToLower(c));
            }

            if (currentWord.Length > 0)
            {
                parts.Add(currentWord.ToString());
            }

            return String.Join(separator, parts.ToArray());
        }
    }

    public class SnakeCasePropertyNamesContractResolver : DeliminatorSeparatedPropertyNamesContractResolver
    {
        public SnakeCasePropertyNamesContractResolver() : base('_') { }
    }
    */
}