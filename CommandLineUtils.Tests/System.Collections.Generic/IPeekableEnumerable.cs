﻿namespace Lobster.System.Collections.Generic
{
    public interface IPeekableEnumerable<T> 
    {
        IPeekableEnumerator<T> GetEnumerator();
    }
}