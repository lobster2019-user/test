using NUnit.Framework;
using Jaeger;
using Jaeger.Reporters;
using Jaeger.Samplers;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging.Console;
using OpenTracing.Tag;
using OpenTracing.Propagation;
using System.Collections.Generic;
using System.Threading;

namespace Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }



        [Test]
        public async Task Test1()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddLogging(builder => builder
                    .AddConsole()
                    .SetMinimumLevel(LogLevel.Trace));
            using (var serviceProvider = serviceCollection.BuildServiceProvider())
            {
                using (var loggerFactory = serviceProvider.GetService<ILoggerFactory>())
                {
                    var serviceName = "jaeger_tests";
                    var reporter = new CompositeReporter(
                          new MyLoggingReporter(loggerFactory),
                          new RemoteReporter.Builder()
                          .WithLoggerFactory(loggerFactory).WithSender(
                              new MySender(new Jaeger.Senders.UdpSender(), loggerFactory.CreateLogger<MySender>()))//
                                                                                                                   //new HttpSender("http://127.0.0.1:14268/traces"))
                              .Build());
                    var reporter2 = new CompositeReporter(
                          new MyLoggingReporter(loggerFactory),
                          new RemoteReporter.Builder()
                          .WithLoggerFactory(loggerFactory).WithSender(
                              new MySender(new Jaeger.Senders.UdpSender(), loggerFactory.CreateLogger<MySender>()))//
                                                                                                                   //new HttpSender("http://127.0.0.1:14268/traces"))
                              .Build());


                    ISampler sampler = new RemoteControlledSampler.Builder(serviceName)
                                    .WithLoggerFactory(loggerFactory)
                                    .WithSamplingManager(new HttpSamplingManager())
                                    .Build();
                    ISampler sampler2 = new RemoteControlledSampler.Builder(serviceName + "-3")
                                    .WithLoggerFactory(loggerFactory)
                                    .WithSamplingManager(new HttpSamplingManager())
                                    .Build();
                    sampler = new ConstSampler(true);
                    using (var tracer = new Tracer.Builder(serviceName)
                        .WithLoggerFactory(loggerFactory)
                        .WithReporter(reporter)
                        .WithSampler(sampler)
                        .Build())
                    {

                        {
                            try
                            {
                                var spanBuilder = tracer.BuildSpan("op1");
                                spanBuilder.WithTag(new StringTag("tag1"), "value1");
                                using (var scope = spanBuilder.StartActive(true))
                                {
                                    var span = scope.Span.SetBaggageItem("key-bag1", "value-bag1");
                                    span.Log("log1-1");
                                    var spanBuilder2 = tracer.BuildSpan("op1->op2");
                                    spanBuilder2.WithTag(new StringTag("tag2"), "value2");
                                    using (var scope2 = spanBuilder2.StartActive(true))
                                    {
                                        var span2 = scope2.Span;
                                        span2.Log("log2-1");
                                        await Task.Delay(1000);



                                        var map = new Dictionary<string, string>();

                                        Tags.Component.Set(span2, "example-client");
                                        Tags.SpanKind.Set(span2, Tags.SpanKindClient);
                                        tracer.Inject(span2.Context, BuiltinFormats.TextMap, new TextMapInjectAdapter(map));

                                        var thread = new Thread(() =>
                                        {
                                            using (var tracer2 = new Tracer.Builder(serviceName + "-2")
                            .WithLoggerFactory(loggerFactory)
                            .WithReporter(reporter2)
                            .WithSampler(sampler2)
                            .Build())
                                            {
                                                var context = tracer2.Extract(BuiltinFormats.TextMap, new TextMapExtractAdapter(map));

                                                var spanBuilder34 = tracer2.BuildSpan("op3->op4");
                                                spanBuilder34.WithTag(new StringTag("tag3"), "value3");
                                                spanBuilder34.WithTag(Tags.SpanKind.Key, Tags.SpanKindServer);

                                                using (var scope34 = spanBuilder34.AsChildOf(context)
                                                             /*new SpanContext(
                                                             traceId: TraceId.FromString(span2.Context.TraceId),
                                                             spanId: SpanId.NewUniqueId(),
                                                             parentId: SpanId.FromString(span2.Context.SpanId),
                                                              flags: SpanContextFlags.Sampled))*/.StartActive(true))
                                                {
                                                    var span34 = scope34.Span;
                                                    span34.Log("log3-1");
                                                    Task.Delay(1000).Wait();
                                                    span34.Log("log3-2");
                                                }
                                            }
                                        });
                                        thread.Start();

                                        var spanBuilder3 = tracer.BuildSpan("op1->op2->op3");
                                        spanBuilder3.WithTag(new StringTag("tag3"), "value3");
                                        using (var scope3 = spanBuilder3.StartActive(true))
                                        {
                                            var span3 = scope3.Span;
                                            span3.Log("log3-1");
                                            await Task.Delay(1000);
                                            span3.Log("log3-2");
                                        }

                                        thread.Join();

                                        await Task.Delay(1000);
                                        span2.Log("log2-2");
                                    }
                                    await Task.Delay(2000);
                                    span.Log("log1-2");
                                    Tags.Error.Set(span, true);

                                    var logs = new Dictionary<string, object>();

                                    logs["error.object"] = new System.InvalidCastException();
                                    logs["event"] = "error";
                                    logs["message"] = new System.InvalidCastException().ToString();

                                    span.Log(logs);

                                    await Task.Delay(1000);
                                }
                            }
                            finally
                            {

                            }

                        }
                    }
                }
            }
        }
    }
}