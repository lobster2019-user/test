﻿using NLog;
using System;
using System.Diagnostics;
using System.Reflection;
using System.Threading.Tasks;

namespace Tests
{
    [AttributeUsage(AttributeTargets.Method)]
    public class LogInterceptorAttribute : 

        MethodDecorator.Fody.Interfaces.MethodDecoratorAttribute
    {

        private bool? _isAsync = null;
        private ILogger _logger = null;
        private string _methodName;

        public override void Init(object instance, MethodBase method, object[] args)
        {
            _logger = LogManager.GetLogger(method.DeclaringType.FullName);
            _methodName = method.Name;
            Debug.WriteLine($"{_methodName}: Init");

            if(method is MethodInfo mi)
            {
                if (typeof(Task).IsAssignableFrom(mi.ReturnType))
                {
                    _isAsync = true;
                }
                else
                {
                    _isAsync = false;
                }
            }
            else
            {
                throw new NotSupportedException($"{method.GetType().FullName}");
            }
        }

        public override void OnEntry()
        {
            if (_isAsync.HasValue)
            {
                _logger.Trace($"+{_methodName}");
                ScopedContext.Enter();
            }
        }

        public override void OnException(Exception exception)
        {
            if (_isAsync.HasValue)
            {
                _logger.Error(exception, $"sync");
            }
        }

        public override void OnExit()
        {
            if (_isAsync == false)
            {
               _logger.Trace($"-{_methodName}");
                ScopedContext.Exit();
            }
        }
        public override void OnTaskContinuation(Task task)
        {
            //base.OnTaskContinuation(task);
            if(_isAsync != true)
            {
                throw new InvalidOperationException();
            }

            Debug.WriteLine($"{_methodName}: OnTaskContinuation");
            task.ContinueWith(t =>
            {
                try
                {
                    ScopedContext.Exit();
                    
                }
                finally
                {
                    if (t.IsFaulted)
                    {
                        _logger.Error(t.Exception, $"-{_methodName}: async-exception");
                    }
                    else if (t.IsCanceled)
                    {
                        _logger.Warn($"-{_methodName}: async-canceled");
                    }
                    else if (t.IsCompleted)
                    {
                        _logger.Trace($"-{_methodName}");
                    }
                }
                
            });
        }
    }
}