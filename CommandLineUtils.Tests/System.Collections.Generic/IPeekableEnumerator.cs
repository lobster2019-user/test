﻿using System.Collections.Generic;

namespace Lobster.System.Collections.Generic
{
    public interface IPeekableEnumerator<T> : IEnumerator<T>
    {
        bool CanPeek { get; }
        T Peek { get; }
    }
}