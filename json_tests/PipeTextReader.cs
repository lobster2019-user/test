﻿using System;
using System.IO;
using System.IO.Pipelines;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    public class PipeTextReader : TextReader
    {
        private readonly PipeReader _reader;
        private readonly Decoder _decoder;

        public PipeTextReader(PipeReader reader, Encoding encoding)
        {
            _decoder = encoding.GetDecoder();
            _reader = reader;
        }

        public override int Read() => throw new NotImplementedException();
        public override int Read(char[] buffer, int index, int count) => throw new NotImplementedException();

        public override async Task<Int32> ReadAsync(
            char[] buffer, int offset, int count)
        {
            Console.WriteLine($"PipeTextReader:ReadAsync(offset={offset},count={count}");
            Int32 decoded = 0;
            ReadResult result;

            do
            {
                result = await _reader.ReadAsync();
                decoded = _decoder.Convert(
                    result.Buffer,
                    new Span<char>(buffer, offset, count),
                    result.IsCompleted,
                    out var consumed);
                _reader.AdvanceTo(consumed);
            }
            while (decoded == 0 && !result.IsCompleted);

            Console.WriteLine($"PipeTextReader:ReadAsync-Return(result={decoded},offset={offset},count={count}");
            return decoded;
        }
    }
}