﻿using EasyHook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace injected_dll
{
    public class InjectedClass : EasyHook.IEntryPoint
    {
        private bool MyBeep(uint _)
        {
            MessageBox.Show($"{_}");
            return true;
        }

        [UnmanagedFunctionPointer(CallingConvention.StdCall, SetLastError = true)]
        delegate bool MessageBeepDelegate(uint _);

        [DllImport("user32.dll")]
        static extern bool MessageBeep(uint _);

        private LocalHook _hook;
        public InjectedClass(RemoteHooking.IContext InContext)
        {
  
        }
        public void Run(RemoteHooking.IContext context)
        {
            _hook = LocalHook.Create(
                    LocalHook.GetProcAddress("user32.dll", "MessageBeep"),
                    new MessageBeepDelegate(MyBeep),
                    null);

            MessageBeep(0);

            _hook.ThreadACL.SetExclusiveACL(new Int32[0]);

            var thread = new Thread(() => MessageBeep(0));
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            System.Threading.Thread.Sleep(-1);
        }
    }
}
