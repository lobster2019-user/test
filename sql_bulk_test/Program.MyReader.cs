﻿using System;
using System.Collections;
using System.Data;
using System.Data.Common;

namespace sql_bulk_test
{
    partial class Program
    {
        public class MyReader : DbDataReader
        {
            private readonly int _count;

            public MyReader(Int32 count)
            {
                _count = count;
            }

            public override object this[int i] => throw new NotImplementedException();

            public override object this[string name] => throw new NotImplementedException();

            public override int Depth => throw new NotImplementedException();

            public override bool IsClosed => throw new NotImplementedException();

            public override int RecordsAffected => throw new NotImplementedException();

            public override int FieldCount => 1;

            public override bool HasRows => throw new NotImplementedException();

            public override void Close()
            {
                throw new NotImplementedException();
            }

            public override bool GetBoolean(int i)
            {
                throw new NotImplementedException();
            }

            public override byte GetByte(int i)
            {
                throw new NotImplementedException();
            }

            public override long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }

            public override char GetChar(int i)
            {
                throw new NotImplementedException();
            }

            public override long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
            {
                throw new NotImplementedException();
            }


            public override string GetDataTypeName(int i)
            {
                throw new NotImplementedException();
            }

            public override DateTime GetDateTime(int i)
            {
                throw new NotImplementedException();
            }

            public override decimal GetDecimal(int i)
            {
                throw new NotImplementedException();
            }

            public override double GetDouble(int i)
            {
                throw new NotImplementedException();
            }

            public override Type GetFieldType(int i)
            {
                throw new NotImplementedException();
            }

            public override float GetFloat(int i)
            {
                throw new NotImplementedException();
            }

            public override Guid GetGuid(int i)
            {
                throw new NotImplementedException();
            }

            public override short GetInt16(int i)
            {
                throw new NotImplementedException();
            }

            public override int GetInt32(int i)
            {
                throw new NotImplementedException();
            }

            public override long GetInt64(int i)
            {
                throw new NotImplementedException();
            }

            public override string GetName(int i)
            {
                throw new NotImplementedException();
            }

            public override int GetOrdinal(string name)
            {
                throw new NotImplementedException();
            }

            public override DataTable GetSchemaTable()
            {
                throw new NotImplementedException();
            }

            public override string GetString(int i)
            {
                if (_index < 0 || _index >= _count)
                    throw new InvalidOperationException();
                return _index.ToString();
            }

            public override object GetValue(int i)
            {
                throw new InvalidOperationException();
            }

            public override int GetValues(object[] values)
            {
                throw new NotImplementedException();
            }

            public override bool IsDBNull(int i)
            {
                return false;
            }

            public override bool NextResult()
            {
                throw new NotImplementedException();
            }

            private Int32 _index = -1;

            public override bool Read()
            {
                if (_count > 0 && _index + 1 < _count)
                {
                    ++_index;
                    return true;
                }
                return false; 
            }

            public override IEnumerator GetEnumerator()
            {
                throw new NotImplementedException();
            }
        }
    }
}
