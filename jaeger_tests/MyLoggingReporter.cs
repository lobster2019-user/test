﻿using Jaeger;
using Jaeger.Reporters;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using System.Threading;
using System;
using System.Linq;

namespace Tests
{
    public class MyLoggingReporter : IReporter
    {
        private ILogger Logger { get; }

        public MyLoggingReporter(ILoggerFactory loggerFactory)
        {
            Logger = loggerFactory?.CreateLogger<LoggingReporter>() ?? throw new ArgumentNullException(nameof(loggerFactory));
        }

        public void Report(Span span)
        {
            Logger.LogInformation("Span reported: {span} ; tags: {tags}", span,
                string.Join(", ", span.GetTags().Select(z=>$"{z.Key}={z.Value}")));
            foreach (var log in span.GetLogs())
            {
                Logger.LogInformation($"{log.TimestampUtc} : {log.Message}");
            }
        }

        public Task CloseAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public override string ToString()
        {
            return $"{nameof(MyLoggingReporter)}(Logger={Logger})";
        }
    }
}