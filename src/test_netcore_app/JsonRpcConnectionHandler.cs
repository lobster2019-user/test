﻿using Microsoft.AspNetCore.Connections;
using Microsoft.AspNetCore.Http.Connections.Internal.Transports;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.SignalR.Internal;
using Microsoft.AspNetCore.SignalR.Protocol;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace signalr__server
{/*
    public class JsonRpcConnectionHandler<THub> : HubConnectionHandler<THub>
        where THub : Hub
    {
        public const string SecWebSocketProtocol = "Sec-WebSocket-Protocol";
        public const string JsonRpcProtocol2 = "jsonrpc/2.0";
        private readonly HubLifetimeManager<THub> _lifetimeManager;
        private readonly HubOptions<THub> _hubOptions;
        private readonly HubOptions _globalHubOptions;
        private readonly ILoggerFactory _loggerFactory;
        private readonly HubDispatcher<THub> _dispatcher;

        public JsonRpcConnectionHandler(
            HubLifetimeManager<THub> lifetimeManager, 
            IHubProtocolResolver protocolResolver, 
            IOptions<HubOptions> globalHubOptions, 
            IOptions<HubOptions<THub>> hubOptions, 
            ILoggerFactory loggerFactory, 
            IUserIdProvider userIdProvider, 
            HubDispatcher<THub> dispatcher)
            : base(lifetimeManager, 
                  protocolResolver, 
                  globalHubOptions, 
                  hubOptions, 
                  loggerFactory, 
                  userIdProvider, 
                  dispatcher)
        {
            _lifetimeManager = lifetimeManager;
            _globalHubOptions = globalHubOptions.Value;
            _hubOptions = hubOptions.Value;
            _loggerFactory = loggerFactory;
            _dispatcher = dispatcher;
        }



        public override Task OnConnectedAsync(ConnectionContext connection)
        {
            var request = connection.Features.Get<IHttpRequestFeature>();
            var protocol = request.Headers[SecWebSocketProtocol];
            if (protocol == JsonRpcProtocol2)
            {
                connection.Features.Set<ConnectionContext>(connection);
                return OnJsonRpcConnectedAsync(connection);
            }
            return base.OnConnectedAsync(connection);
        }

        private async Task OnJsonRpcConnectedAsync(ConnectionContext connection)
        { 
            // Then set the keepAlive and handshakeTimeout values to the defaults in HubOptionsSetup incase they were explicitly set to null.
            var keepAlive = _hubOptions.KeepAliveInterval ?? _globalHubOptions.KeepAliveInterval ?? HubOptionsSetup.DefaultKeepAliveInterval;
            var clientTimeout = _hubOptions.ClientTimeoutInterval ?? _globalHubOptions.ClientTimeoutInterval ?? HubOptionsSetup.DefaultClientTimeoutInterval;
            var handshakeTimeout = _hubOptions.HandshakeTimeout ?? _globalHubOptions.HandshakeTimeout ?? HubOptionsSetup.DefaultHandshakeTimeout;
            var supportedProtocols = _hubOptions.SupportedProtocols ?? _globalHubOptions.SupportedProtocols;

            var connectionContext = new HubConnectionContext(connection, 
                keepAlive, 
                _loggerFactory, 
                clientTimeout);

            try
            {
                await _lifetimeManager.OnConnectedAsync(connectionContext);
                await RunHubAsync(connectionContext);
            }
            finally
            {
                await _lifetimeManager.OnDisconnectedAsync(connectionContext);
            }
        }

        private async Task RunHubAsync(HubConnectionContext connection)
        {
            try
            {
                await _dispatcher.OnConnectedAsync(connection);
            }
            catch (Exception ex)
            {
                //Log.ErrorDispatchingHubEvent(_logger, "OnConnectedAsync", ex);

                await SendCloseAsync(connection, ex);

                // return instead of throw to let close message send successfully
                return;
            }

            try
            {
                await DispatchMessagesAsync(connection);
            }
            catch (OperationCanceledException)
            {
                // Don't treat OperationCanceledException as an error, it's basically a "control flow"
                // exception to stop things from running
            }
            catch (Exception ex)
            {
                //Log.ErrorProcessingRequest(_logger, ex);

                await HubOnDisconnectedAsync(connection, ex);

                // return instead of throw to let close message send successfully
                return;
            }

            await HubOnDisconnectedAsync(connection, null);
        }

        private async Task HubOnDisconnectedAsync(HubConnectionContext connection, Exception exception)
        {
            // send close message before aborting the connection
            await SendCloseAsync(connection, exception);

            // We wait on abort to complete, this is so that we can guarantee that all callbacks have fired
            // before OnDisconnectedAsync

            // Ensure the connection is aborted before firing disconnect
            connection.Abort();//.AbortAsync();

            try
            {
                await _dispatcher.OnDisconnectedAsync(connection, exception);
            }
            catch (Exception ex)
            {
                //Log.ErrorDispatchingHubEvent(_logger, "OnDisconnectedAsync", ex);
                throw;
            }
        }

        private async Task SendCloseAsync(HubConnectionContext connection, Exception exception)
        {
            var closeMessage = CloseMessage.Empty;

            if (exception != null)
            {
                var errorMessage = "???";// ErrorMessageHelper.BuildErrorMessage("Connection closed with an error.", exception, _enableDetailedErrors);
                closeMessage = new Microsoft.AspNetCore.SignalR.Protocol.CloseMessage(errorMessage);
            }

            try
            {
                await connection.WriteAsync(closeMessage);
            }
            catch (Exception ex)
            {
                //Log.ErrorSendingClose(_logger, ex);
            }
        }

        private async Task DispatchMessagesAsync(HubConnectionContext connection)
        {
            var input = connection.Features.Get<ConnectionContext>().Transport.Input;
            var protocol = connection.Protocol;
            while (true)
            {
                var result = await input.ReadAsync();
                var buffer = result.Buffer;

                try
                {
                    if (result.IsCanceled)
                    {
                        break;
                    }

                    if (!buffer.IsEmpty)
                    {
                        //connection.ResetClientTimeout();

                        while (protocol.TryParseMessage(ref buffer, _dispatcher, out var message))
                        {
                            await _dispatcher.DispatchMessageAsync(connection, message);
                        }
                    }

                    if (result.IsCompleted)
                    {
                        if (!buffer.IsEmpty)
                        {
                            throw new InvalidDataException("Connection terminated while reading a message.");
                        }
                        break;
                    }
                }
                finally
                {
                    // The buffer was sliced up to where it was consumed, so we can just advance to the start.
                    // We mark examined as buffer.End so that if we didn't receive a full frame, we'll wait for more data
                    // before yielding the read again.
                    input.AdvanceTo(buffer.Start, buffer.End);
                }
            }
        }
    }*/
}
