using FluentValidation;
using NUnit.Framework;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation.TestHelper;
using System.Collections.Generic;
using FluentValidation.Results;

namespace Tests
{
    public static class _ex
    {
        public static IEnumerable<ValidationFailure> WhenAll(
            this IEnumerable<ValidationFailure> failures,
            Func<ValidationFailure, bool> failurePredicate, string exceptionMessage = null)
        {
            bool allMatched = failures.All(failurePredicate);

            if (!allMatched)
            {
                var failure = failures.First();

                string message = "Expected validation error was not found";

                if (exceptionMessage != null)
                {
                    message = exceptionMessage.Replace("{Code}", failure.ErrorCode)
                        .Replace("{Message}", failure.ErrorMessage)
                        .Replace("{State}", failure.CustomState?.ToString() ?? "")
                        .Replace("{Severity}", failure.Severity.ToString());
                }

                throw new ValidationTestException(message);
            }

            return failures;
        }

        public static IEnumerable<ValidationFailure> WithoutErrorMessage(
            this IEnumerable<ValidationFailure> failures, string expectedErrorMessage)
        {
            return failures.WhenAll(failure => failure.ErrorMessage != expectedErrorMessage, string.Format("Expected an error message of '{0}'. Actual message was '{{Message}}'", expectedErrorMessage));
        }
    }
    public class Tests
    {
        private IValidator<ABC> _validator;
        private IValidator<ABC> _validator2;

        [OneTimeSetUp]
        public void Setup()
        {
            _validator = new AbcValidator();
            _validator2 = new AbcValidator2();
        }
        void AssertValid<T>(IValidator<T> validator, T arg)
        {
            Assert.True(validator.Validate(arg).IsValid);
        }
        async Task AssertValidAsync<T>(IValidator<T> validator, T arg)
        {
            Assert.True((await validator.ValidateAsync(arg)).IsValid);
        }
        void AssertInvalid<T>(IValidator<T> validator, T arg)
        {
            var result = validator.Validate(arg);
            Console.WriteLine(string.Join("\r\n", result.Errors.Select(z => z.ErrorMessage)));
            Assert.False(result.IsValid);
        }
        void AssertInvalidWithMessage<T>(IValidator<T> validator, T arg, string msg)
        {
            var result = validator.Validate(arg);
            Console.WriteLine(string.Join("\r\n", result.Errors.Select(z => z.ErrorMessage)));
            Assert.True(result.Errors.Any(z => z.ErrorMessage.IndexOf(msg) >= 0));
            Assert.False(result.IsValid);
        }
        void AssertInvalidWithoutMessage<T>(IValidator<T> validator, T arg, string msg)
        {
            var result = validator.Validate(arg);
            Console.WriteLine(string.Join("\r\n", result.Errors.Select(z => z.ErrorMessage)));
            Assert.True(result.Errors.All(z => z.ErrorMessage.IndexOf(msg) < 0));
            Assert.False(result.IsValid);
        }
        [Test]
        public void Test3()
        {
            _validator2.TestValidate(new ABC { A = 99, B = 199, C = 777 })
                .ShouldHaveError()
                .WithErrorMessage("condition: A>100")
                .WithoutErrorMessage("condition: B>100");

            _validator2.TestValidate(new ABC { A = 199, B = 99, C = 777 })
                .ShouldHaveError()
                .WithErrorMessage("condition: B>100")
                .WithoutErrorMessage("condition: A>100");


            _validator2.TestValidate(new ABC { A = 99, B = 99, C = 777 })
                .ShouldHaveError()
                .WithErrorMessage("condition: A>100")
                .WithErrorMessage("condition: B>100");

            _validator2.TestValidate(new ABC { A = 199, B = 199, C = 777 })
                .ShouldNotHaveError();

            Assert.Pass();
        }
        [Test]
        public void Test1()
        {
            AssertValid(_validator, new ABC { A = -1, B = -1, C = 0 });
            AssertValid(_validator, new ABC { A = 1, B = 1, C = 0 });
            Assert.Pass();
        }
        [Test]
        public void Test2()
        {
            _validator.TestValidate(new ABC { A = 1, B = -1, C = 0 }).ShouldHaveError();
            _validator.TestValidate(new ABC { A = -1, B = 1, C = 0 }).ShouldHaveError();
        }
        [Test]
        public async Task Test1Async()
        {
            await AssertValidAsync(_validator, new ABC { A = -1, B = -1, C = 0 });
            Assert.Pass();
        }
    }
}