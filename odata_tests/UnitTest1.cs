using Microsoft.Data.Edm;
using NUnit.Framework;
using Simple.OData.Client;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Tests
{
    //https://csharp.hotexamples.com/ru/examples/Simple.OData.Client/ODataClient/-/php-odataclient-class-examples.html
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }
        [Test]
        public async Task MicrosoftOdataClientTest()
        {
            //var client = new DemoService(new Uri("http://services.odata.org/V4/OData/OData.svc/"));
            //client.
        }

        string GetClrType(IEdmTypeReference type)
        {
            return type.FullName().Split('.').Last();
        }

        [Test]
        public async Task SimpleOdataClientTest()
        {
            var http = new HttpClient(new LogHttpMessageHandler(new HttpClientHandler()
            {
                ServerCertificateCustomValidationCallback = (a, b, c, d) => true,
                Proxy = new WebProxy("http://127.0.0.1:8888/"),
            }));
            var client = new ODataClient(new ODataClientSettings
                (http)
            {                 
                PayloadFormat = ODataPayloadFormat.Json,
                BaseUri = new Uri("https://www.nuget.org/api/v2"),
            });

            //(CsdlSemanticsModel.CsdlSemanticsModel)
            var metadata = await client.GetMetadataAsync<IEdmModel>();
            var entityTypes = metadata.SchemaElements.OfType<IEdmEntityType>().ToArray();
            foreach (var type in entityTypes)
            {
                Console.WriteLine($"namespace {type.Namespace}");
                Console.WriteLine("{");
                Console.WriteLine($"\tpublic class {type.Name}");
                Console.WriteLine("\t{");
                foreach(var prop in type.Properties())
                {
                    Console.WriteLine($"\t\tpublic {GetClrType(prop.Type)} {prop.Name} {{get; set;}}");
                }
               
                Console.WriteLine("\t}");
                Console.WriteLine("}");

           }

            var packages = await client
                    .FindEntriesAsync("Packages?$filter=Title eq 'System.Net.Http'");
            foreach (var package in packages)
            {
                foreach(var kv in package)
                {
                    //Console.WriteLine($"{kv.Key}={kv.Value}");
                }
                //Console.WriteLine();
            }

            var result = await client
                .For<Package>()
                .Filter(z => z.Title == "System.Net.Http" && z.Version == "4.3.2")
                .FindEntryAsync();
            Console.WriteLine($"{result.Title} {result.Version}");
        }
    }
    public class Package
    {
        public string Title { get; set; }
        public string Version { get; set; }
    }
}