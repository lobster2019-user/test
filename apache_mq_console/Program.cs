﻿using Amqp;
using Amqp.Framing;
using Amqp.Handler;
using Apache.NMS;
using Apache.NMS.AMQP.Util;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;

namespace apache_mq_console
{
    class Program
    {
        static void Main(string[] args)
        {
            Amqp2ProviderFactory.Register();
            var threads = new List<Thread>();
            for (var j = 0; j < 2; ++j)
            {
                var jj = j;
                var thread = new Thread(() => CreateSharedDurableQueue("client-0-" + jj));
                //var thread = new Thread(() => ReceiveFromQueue("client-0-" + jj));
                thread.Start();
                threads.Add(thread);
                System.Threading.Thread.Sleep(500);
            }
            foreach (var thread in threads)
            {
                thread.Join();
            }
        }
        public class ClearClientIdHandler : IHandler
        {
            public bool CanHandle(EventId id)
            {
                return id == EventId.ConnectionLocalOpen;
            }

            public void Handle(Event protocolEvent)
            {
                if (protocolEvent.Context is Open open)
                {
                    open.ContainerId = "";
                }
            }
        }

        internal enum TerminusDurability
        {
            NONE = 0,
            CONFIGURATION = 1,
            UNSETTLED_STATE = 2,
        }

        static void CreateSharedDurableQueue(string clientId)
        {
            var connection = new Connection(new Address("amqp://localhost:61616/"));
            connection.Closed += (s, e) =>
            {
                Console.WriteLine("closed");
            };
            var session = new Session(connection);
            Attach attach = new Attach()
            {

                Source = new Source()
                {
                    Address = "pubsub.foo",
                    Capabilities = new[]
                    {
                        SymbolUtil.ATTACH_CAPABILITIES_TOPIC, SymbolUtil.SHARED
                    },
                    ExpiryPolicy = SymbolUtil.ATTACH_EXPIRY_POLICY_NEVER,
                    Durable = (Int32)TerminusDurability.UNSETTLED_STATE,
                    DistributionMode = SymbolUtil.ATTACH_DISTRIBUTION_MODE_COPY,
                    DefaultOutcome = MessageSupport.MODIFIED_FAILED_INSTANCE,
                    Outcomes = new[]
                    {
                        SymbolUtil.ATTACH_OUTCOME_ACCEPTED,
                        SymbolUtil.ATTACH_OUTCOME_RELEASED,
                        SymbolUtil.ATTACH_OUTCOME_REJECTED,
                        SymbolUtil.ATTACH_OUTCOME_MODIFIED
                    }
                },
                Target = new Target() { },
                RcvSettleMode = ReceiverSettleMode.First,
                SndSettleMode = SenderSettleMode.Unsettled,
            };

            var receiver = new ReceiverLink(session, clientId + ".pubsub.foo", attach, (l, a) =>
           {
               Console.WriteLine($"{l} {a}");
           });
            receiver.Start(0);
            System.Threading.Thread.Sleep(100);
        }

        static void ReceiveFromTopic(string client)
        {
            var url = $"{Amqp2ProviderFactory.Schema}://localhost:61616/?consumerWindowSize=1";

            var factory = new Apache.NMS.AMQP.NmsConnectionFactory(url);

            factory.ClientId = client;
            factory.ClientIdPrefix = client;
            factory.ConnectionIdPrefix = client;

            using (var connection = factory.CreateConnection())
            {
                connection.ExceptionListener += Connection_ExceptionListener;
                using (var session = connection.CreateSession(AcknowledgementMode.ClientAcknowledge))
                {
                    connection.Start();
                    using (var q1 = session.GetTopic("pubsub.foo"))
                    {
                        using (var consumer = session.CreateDurableConsumer(q1, "pubsub.foo", selector: null, noLocal: false))
                        {
                            for (var i = 0; i < 10; ++i)
                            {
                                var r2 = consumer.Receive(TimeSpan.FromSeconds(3));
                                Console.WriteLine($"{client}: {(r2 != null ? r2.ToString() : "<null>") }");
                                System.Threading.Thread.Sleep(2000);
                                r2?.Acknowledge();
                            }
                        }
                    }
                }
            }
        }
        static void ReceiveFromQueue(string client)
        {
            var url = $"{Amqp2ProviderFactory.Schema}://localhost:61616/?consumerWindowSize=1";

            var factory = new Apache.NMS.AMQP.NmsConnectionFactory(url);
            factory.ClientId = client;
            using (var connection = factory.CreateConnection())
            {
                connection.ExceptionListener += Connection_ExceptionListener;
                using (var session = connection.CreateSession(AcknowledgementMode.ClientAcknowledge))
                {
                    connection.Start();
                    using (var q1 = session.GetQueue("pubsub.foo::client-0-0.pubsub.foo:global"))
                    {
                        using (var consumer = session.CreateConsumer(q1, selector: "x = '10'"))
                        {
                            for (var i = 0; i < 10; ++i)
                            {
                                var r2 = consumer.Receive(TimeSpan.FromSeconds(5));
                                Console.WriteLine($"{client}: {(r2 != null ? r2.ToString() : "<null>") }");
                                System.Threading.Thread.Sleep(2000);
                                r2?.Acknowledge();
                            }
                        }
                    }
                }
            }
        }

        public static void ConnectionTwice()
        {
            /*
                    var factory = new Apache.NMS.AMQP.NmsConnectionFactory();
            factory.ClientId = "clientid-0";
            _connection = factory.CreateConnection();
            _connection.ExceptionListener += _connection_ExceptionListener;
            _session = _connection.CreateSession(AcknowledgementMode.ClientAcknowledge);

             */
            var url = "amqp://localhost:61616?consumerWindowSize=0";
            var factory = new Apache.NMS.AMQP.NmsConnectionFactory(url);

            factory.ClientId = "clientid-1.0";
            using (var connection = factory.CreateConnection())
            {
                connection.ExceptionListener += Connection_ExceptionListener;
                using (var session = connection.CreateSession(AcknowledgementMode.ClientAcknowledge))
                {
                    var factory2 = new Apache.NMS.AMQP.NmsConnectionFactory(url);
                    factory2.ClientId = "clientid-1.1";
                    using (var connection2 = factory2.CreateConnection())
                    {
                        connection2.ExceptionListener += Connection_ExceptionListener;
                        using (var session2 = connection2.CreateSession(AcknowledgementMode.ClientAcknowledge))
                        {

                            connection.Start();
                            connection2.Start();
                            for (var i = 0; i < 2; ++i)
                            {
                                using (var q1 = session.GetQueue("pubsub.foo::client123.pubsub.foo?consumer.prefetchSize=1"))
                                {
                                    using (var consumer = session.CreateConsumer(q1))
                                    {
                                        using (var q2 = session2.GetQueue("pubsub.foo::client123.pubsub.foo?consumer.prefetchSize=1"))
                                        {
                                            using (var consumer2 = session2.CreateConsumer(q2))
                                            {
                                                var r2 = consumer2.Receive(TimeSpan.FromSeconds(1));
                                                Console.WriteLine($"r2: {r2}");
                                                r2?.Acknowledge();

                                                var r1 = consumer.Receive(TimeSpan.FromSeconds(1));
                                                Console.WriteLine($"r1: {r1}");
                                                r1?.Acknowledge();


                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
            }
        }

        private static void Connection_ExceptionListener(Exception exception)
        {
            Console.WriteLine(exception);
        }
    }
}
