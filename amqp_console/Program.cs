﻿using Amqp;
using Amqp.Framing;
using System;
using System.Threading.Tasks;

namespace amqp_console
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var address = "amqp://127.0.0.1:5672/";
            var connection = new Connection(new Address(address))
            {

            };
            
            var session = new Session(connection);
            

            var recvAttach = new Attach()
            {
                Source = new Source() { Address = "pubsub.foo::client123.pubsub.foo" },
                Target = new Target() { Address = this.replyTo }
            };

            var receiver = new ReceiverLink(session, "request-client-receiver", recvAttach, null);
            receiver.Start(0);
            var m = await receiver.ReceiveAsync();
            Console.WriteLine($"Body: {m?.Body}");
        }
    }
}
