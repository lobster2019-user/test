﻿using System;
using System.Collections.Generic;

namespace Lobster.System.Collections.Generic
{
    public static class BufferedEnumerableExtentions
    {
        public static IBufferedEnumerator<T> AsBuffered<T>(this IEnumerator<T> enumerator,
             bool canGoBackward = true, bool canGoForward = true, Int32 maxCacheSize = 0)
            => new BufferedEnumerator<T>(
                enumerator,
                canGoBackward: canGoBackward,
                canGoForward: canGoForward,
                maxCacheSize: maxCacheSize);

        public static IBufferedEnumerable<T> AsBuffered<T>(this IEnumerable<T> enumerable,
            bool canGoBackward = true, bool canGoForward = true, Int32 maxCacheSize = 0)
            => new BufferedEnumerable<T>(
                enumerable,
                canGoBackward: canGoBackward,
                canGoForward: canGoForward,
                maxCacheSize: maxCacheSize);

        public static bool TryPeek<T>(this IBufferedEnumerator<T> enumerator,Int32 count, out T[] result)
        {
            result = new T[count];
            var peeked = enumerator.TryPeek(1, count, result, 0);
            Array.Resize(ref result, peeked);
            return result.Length > 0;
        }
    }
}