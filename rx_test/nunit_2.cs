﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit;

namespace rx_test
{
    public class unit
    {
        [NUnit.Framework.TestCase(1, 2, 3)]
        public void NunitTest_TestCase(Int32 a, Int32 b, Int32 c)
        {
            Console.WriteLine(a);
            Console.WriteLine(b);
        }
        [NUnit.Framework.TestCase(1, 2)]
        public void NunitTest_TestCase2(Int32 a, Int32 b, Int32 c)
        {
            Console.WriteLine(a);
            Console.WriteLine(b);
        }
        [NUnit.Framework.TestCase(1, 2, 3, 4)]
        public void NunitTest_TestCase4(Int32 a, Int32 b, Int32 c)
        {
            Console.WriteLine(a);
            Console.WriteLine(b);
        }
        [NUnit.Framework.Test]
        public void NunitTest_TestCase0(Int32 a, Int32 b, Int32 c)
        {
            Console.WriteLine(a);
            Console.WriteLine(b);
        }
    }
}
