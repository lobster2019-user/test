﻿using CommandLine;
using System;
using System.Linq;
using Tests;

namespace commandline_tests.console
{
    class Program
    {
        public static void Main1(String[] args)
        {
            Console.WriteLine(string.Join(",", args.Select(z => $"({z})")));
            var configuration = new Options_412();
            var parseResult = Parser.Default.ParseArguments<Options_412>(args).WithParsed(result => configuration = result);

            System.Console.WriteLine(parseResult.Tag);
            System.Console.WriteLine(configuration.Description);
        }

        public static void Main(String[] args)
        {
            args = new[] { "-b",
                //"bb", 
                "-v", "BV" };
            Console.WriteLine(string.Join(",", args.Select(z => $"({z})")));
            var configuration = new Options_420();
            var parseResult = Parser.Default.ParseArguments<Options_420>(args)
                .WithParsed(result => configuration = result);

            Console.WriteLine(parseResult.Tag);
            Console.WriteLine(configuration.Bookids != null ? 
                string.Join(",", configuration.Bookids)
                : "<null>");
        }
    }
}
