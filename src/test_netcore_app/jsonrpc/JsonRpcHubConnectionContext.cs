﻿using System;
using System.IO.Pipelines;
using Microsoft.AspNetCore.Connections;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;

namespace test_netcore_app
{
    public class JsonRpcHubConnectionContext :HubConnectionContext
    {
        private readonly ConnectionContext _connectionContext;
        private readonly ILoggerFactory _loggerFactory;

        public JsonRpcHubConnectionContext(ConnectionContext connectionContext,ILoggerFactory loggerFactory) 
            : base(connectionContext,TimeSpan.MinValue,loggerFactory,TimeSpan.MinValue)
        {
            this._connectionContext = connectionContext;
            this._loggerFactory = loggerFactory;
        }

        internal PipeReader Input => _connectionContext.Transport.Input;
    }
}
