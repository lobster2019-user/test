﻿using Grpc.Core;
using System;

namespace grpc_client
{
    class Program
    {
        static void Main(string[] args)
        {
            //Environment.SetEnvironmentVariable("http_proxy", "http://127.0.0.1:8888/");
            GrpcEnvironment.SetEnvironmentVariable("GRPC_TRACE", "all");
            GrpcEnvironment.SetEnvironmentVariable("GRPC_VERBOSITY", "DEBUG");
            GrpcEnvironment.SetEnvironmentVariable("GRPC_TRACE_FUZZER", "");
            GrpcEnvironment.SetLogger(new Grpc.Core.Logging.ConsoleLogger());
            

            Channel channel = new Channel("127.0.0.1:50051", ChannelCredentials.Insecure);
            

            var client = new Greeter.GreeterClient(channel);
            String user = "you";

            var reply = client.SayHello(new HelloRequest
            {
                Name = user
            });

            Console.WriteLine("Greeting: " + reply.Message);

            channel.ShutdownAsync().Wait();

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
