using Amqp;
using Amqp.Framing;
using Amqp.Handler;
using Apache.NMS;
using Apache.NMS.AMQP;
using Apache.NMS.AMQP.Message;
using Apache.NMS.AMQP.Util;
using Apache.NMS.Util;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace Tests
{
    public class MQContext
    {
        public string BrokerUrl { get; set; } = "amqp://localhost:61616/";
        public string User { get; set; } = "";
        public string Pass { get; set; } = "";
        public string Queue { get; set; } = "test-queue";
    }
    public class ActiveMqNetCoreTests
    {

        private readonly MQContext _context = new MQContext();

        public class ClearClientIdHandler : IHandler
        {
            public bool CanHandle(EventId id)
            {
                return id == EventId.ConnectionLocalOpen;
            }

            public void Handle(Event protocolEvent)
            {
                if (protocolEvent.Context is Open open)
                {
                    open.ContainerId = "";
                }
            }
        }
        class UriUtil
        {
            public static Address ToAddress(Uri uri, string username = null, string password = null)
            {
                Address addr = new Address(uri.Host, uri.Port, username, password, "/", uri.Scheme);
                return addr;
            }
        }

        [TestCase("shared-0")]
        [TestCase("shared-1")]
        [TestCase("shared-2")]
        public async Task CreateSharedDurableQueues(string clientId)
        {
            var links = new[] { _context.BrokerUrl };

            if (_context.BrokerUrl.StartsWith("failover:"))
            {
                var startPos = _context.BrokerUrl.IndexOf("(");
                var endPos = _context.BrokerUrl.LastIndexOf(")");
                if (startPos < 0 || endPos < 0 || startPos >= endPos)
                {
                    throw new FormatException(nameof(_context.BrokerUrl));
                }
                links = _context.BrokerUrl.Substring(startPos + 1, endPos - startPos - 1).Split(",");
            }
            if (links.Length == 0)
            {
                throw new FormatException(nameof(_context.BrokerUrl));
            }
            foreach (var link in links)
            {
                Console.WriteLine($"link: {link}");
                var result = new TaskCompletionSource<bool>();

                var connection = new Connection(UriUtil.ToAddress(new Uri(link), _context.User, _context.Pass), new ClearClientIdHandler());
                connection.Closed += (s, e) =>
                {
                    Console.WriteLine("connection:closed");
                    result.TrySetResult(false);
                };
                var session = new Session(connection);
                Attach attach = new Attach()
                {

                    Source = new Source()
                    {
                        Address = _context.Queue,
                        Capabilities = new[]
                        {
                         SymbolUtil.ATTACH_CAPABILITIES_TOPIC, SymbolUtil.SHARED
                    },
                        ExpiryPolicy = SymbolUtil.ATTACH_EXPIRY_POLICY_NEVER,
                        Durable = 2u,
                        DistributionMode = SymbolUtil.ATTACH_DISTRIBUTION_MODE_COPY,
                        DefaultOutcome = MessageSupport.MODIFIED_FAILED_INSTANCE
                    },
                    Target = new Target() { },
                    RcvSettleMode = ReceiverSettleMode.First,
                    SndSettleMode = SenderSettleMode.Unsettled,
                };

                var receiver = new ReceiverLink(session, clientId + "." + _context.Queue, attach, (l, a) =>
                {
                    Console.WriteLine($"onattached: {l} {a}");
                    result.TrySetResult(true);
                });
                receiver.Start(0);
                var successed = await result.Task;
                Console.WriteLine($"result: {successed}");
                if (successed)
                {
                    break;
                }
            }
        }

        [TestCase("shared-0", "node-0", "node-1")]
        [TestCase("shared-1", "node-0", "node-1")]
        [TestCase("shared-2", "node-0", "node-1")]
        public void CreateSubscription(string clientId, string node1, string node2)
        {
            var factory = new NmsConnectionFactory(_context.BrokerUrl);
            factory.ClientId = clientId + "-" + node1;

            var factory2 = new NmsConnectionFactory(_context.BrokerUrl);
            factory2.ClientId = clientId + "-" + node2;

            using (var connection = factory.CreateConnection(_context.User, _context.Pass))
            {
                using (var connection2 = factory2.CreateConnection(_context.User, _context.Pass))
                {
                    using (var session = connection.CreateSession(Apache.NMS.AcknowledgementMode.ClientAcknowledge))
                    {
                        using (var session2 = connection2.CreateSession(Apache.NMS.AcknowledgementMode.ClientAcknowledge))
                        {
                            using (var queue = session.GetQueue(_context.Queue + "::" + clientId + "." + _context.Queue))
                            {
                                using (var queue2 = session2.GetQueue(_context.Queue + "::" + clientId + "." + _context.Queue))
                                {
                                    using (var consumer = session.CreateConsumer(queue, null, true))
                                    {
                                        using (var consumer2 = session2.CreateConsumer(queue2, null, true))
                                        {
                                            var result = consumer.Receive(TimeSpan.FromMilliseconds(1000));
                                            Console.WriteLine($"{clientId}: {result}");
                                            result?.Acknowledge();

                                            var result2 = consumer2.Receive(TimeSpan.FromMilliseconds(1000));
                                            Console.WriteLine($"{clientId}: {result2}");
                                            result?.Acknowledge();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        [TestCase("unshared-0")]
        public void GenerateUnsharedDurableQueues(string clientId)
        {
            var factory = new NmsConnectionFactory(_context.BrokerUrl);
            factory.ClientId = clientId;
            using (var connection = factory.CreateConnection(_context.User, _context.Pass))
            {
                using (var session = connection.CreateSession(Apache.NMS.AcknowledgementMode.ClientAcknowledge))
                {
                    using (var queue = session.GetTopic(_context.Queue))
                    {
                        using (var consumer = session.CreateDurableConsumer(queue, _context.Queue, selector: null, noLocal: true))
                        {

                        }
                    }
                }
            }
        }

        [TestCase("shared-0", "field = 'd'")]
        [TestCase("shared-1", "'field' = 'c'")]
        [TestCase("shared-2", "'field' = 'b'")]
        [TestCase("unshared-0", "'field' = 'a'")]
        public void GetMessageFromQueueLoop(string node, string filter)
        {
            GetMessageFromQueue(node, selector: filter, loop: true);
        }

        [TestCase("shared-0", "field = 'd'")]
        [TestCase("shared-0", "field = 'a'")]
        [TestCase("shared-1", "field = 'c'")]
        [TestCase("shared-2", "field = 'b'")]
        [TestCase("unshared-0", "field = 'a'")]
        public void GetMessageFromQueue(string node, string selector)
        {
            GetMessageFromQueue(node, selector, loop: false);
        }
        public void GetMessageFromQueue(string node, string selector, bool loop)
        {
            Oracle.ManagedDataAccess.Client.OracleCommand cmd;
            cmd.

            Console.WriteLine(node);
            var factory = new NmsConnectionFactory(_context.BrokerUrl);
            factory.ClientId = "";
            using (var connection = factory.CreateConnection(_context.User, _context.Pass))
            {
                connection.ExceptionListener += OnAmqpException;

                //       Console.WriteLine("connection.Start:before");
                connection.Start();

                //     Console.WriteLine("connection.Start:after");
                //     Console.WriteLine("connection.CreateSession:before");
                using (var session = connection.CreateSession(Apache.NMS.AcknowledgementMode.ClientAcknowledge))
                {

                    //        Console.WriteLine("connection.CreateSession:after");
                    //        Console.WriteLine("session.GetQueue:before");
                    var queueName = _context.Queue + "::" + node + "." + _context.Queue;
                    //        Console.WriteLine($"queueName:{queueName}");
                    using (var queue = session.GetQueue(queueName))
                    {

                        //           Console.WriteLine("session.GetQueue:after");
                        //           Console.WriteLine("session.CreateDurableConsumer:before");
                        //var consumerName = _context.Queue;
                        //            Console.WriteLine($"consumer:{queueName}");
                        using (var consumer = session.CreateConsumer(queue, selector: selector))
                        {

                            //                Console.WriteLine("session.CreateConsumer:after");
                            while (true)
                            {
                                //                   Console.WriteLine("consumer.Receive:before");
                                var receive = consumer.Receive(TimeSpan.FromSeconds(2));
                                //                    Console.WriteLine("consumer.Receive:after");
                                //                   Console.WriteLine(receive?.ToString());
                                if (!loop)
                                {
                                    Assert.NotNull(receive);
                                }
                                if (receive != null)
                                {
                                    Console.WriteLine($"{node}: {receive}");
                                    receive.Acknowledge();
                                }
                                if (!loop)
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        [TestCase("a")]
        [TestCase("b")]
        [TestCase("c")]
        [TestCase("d")]
        [TestCase("z")]
        public void SendMessageToQueue(string value)
        {
            var factory = new NmsConnectionFactory(_context.BrokerUrl);
            factory.ClientId = "";
            using (var connection = factory.CreateConnection(_context.User, _context.Pass))
            {
                using (var session = connection.CreateSession(Apache.NMS.AcknowledgementMode.ClientAcknowledge))
                {
                    using (var queue = session.GetTopic(_context.Queue))
                    {
                        using (var producer = session.CreateProducer(queue))
                        {
                            var msg = session.CreateTextMessage(DateTime.Now.ToString());
                            msg.Properties.SetString("field", value);
                            producer.Send(msg);
                            Console.WriteLine("Sent :{0}", msg);
                        }
                    }
                }
            }
        }


        private void OnAmqpException(System.Exception exception)
        {
            System.Console.WriteLine(exception);
        }
    }
}
