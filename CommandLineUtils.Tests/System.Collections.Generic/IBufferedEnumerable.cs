﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lobster.System.Collections.Generic
{
    public interface IBufferedEnumerable<T>
    {
        IBufferedEnumerator<T> GetEnumerator();
    }
}
