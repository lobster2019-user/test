using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Http;
using NUnit.Framework;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging.Console;
using Microsoft.Extensions.Logging;
using System;

namespace Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public async Task HttpClientFactoryTest()
        {
            var url = "https://www.instagram.com/?hl=ru";

            var services = new ServiceCollection();
           

            services.AddOptions<HttpClientFactoryOptions>();
            services.Configure<HttpClientFactoryOptions>(o =>
            {
                Console.WriteLine("configure");
                o.SuppressHandlerScope = false;
            });

            services.AddHttpClient();
            services.AddHttpClient("name");
            services.AddHttpClient("name2");

            

            var provider = services.BuildServiceProvider();

            for (var i = 0; i < 10; ++i)
            {
                var scope = provider.CreateScope();
                {
                    IHttpClientFactory factory = scope.ServiceProvider.GetRequiredService<IHttpClientFactory>();
                    var http = factory.CreateClient("name");
                    var http2 = factory.CreateClient("name2");
                    await http.GetStringAsync(url);
                    await http2.GetStringAsync(url);
                }
            }
            await Task.Delay(10000);
        }
    }
}