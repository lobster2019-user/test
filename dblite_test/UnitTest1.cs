using LiteDB;
using System;
using System.Collections;
using System.Diagnostics;
using System.Linq;
using Xunit;
using Xunit.Abstractions;
using Xunit.Sdk;

namespace dblite_test
{
    public class UnitTest1
    {
        public class TestClass
        {
            public TestClass(Int32 size)
            {
                Data = new byte[size];
            }
            public string Id { get; set; }
            public byte[] Data { get; }
        }

        private readonly ITestOutputHelper _output;
        public UnitTest1(ITestOutputHelper output)
        {
            _output = output;
        }

        [Theory(Skip = "Skip")]
        [InlineData(@"filename=MyData1.db; journal=false; exclusive=true", 200000, 10, 10000)]
        public void Test1(string dbname, Int32 iterCount, Int32 repeatCount, Int32 count)
        {

            // Open database (or create if doesn't exist)
            using (var db = new LiteDatabase(dbname))
            {
                // Get customer collection
                db.DropCollection("customers");
                var col = db.GetCollection<TestClass>("customers");
                var template = Enumerable.Range(0, count)
                                .Select(z => new TestClass(1024))
                                .ToArray();
                for (var j = 0; j < repeatCount; ++j)
                {
                    for (var i = 0; i < iterCount; i += count)
                    {
                        for (var k = 0; k < count; ++k)
                        {
                            template[k].Id = (k + i) + "#" + j;
                        }

                        col.InsertBulk(template);
                    }
                    _output.WriteLine("{0} {1} mb", j, System.Environment.WorkingSet / 1024 / 1024);
                    //GC.Collect();
                    //GC.WaitForPendingFinalizers();
                    //GC.Collect();
                    //_output.WriteLine("{0} {1} mb", j, System.Environment.WorkingSet / 1024 / 1024);
                }
            }
        }
    }

    public class F1 : IDisposable
    {
        private readonly IMessageSink _diagnosticMessageSink;

        public F1(IMessageSink diagnosticMessageSink)
        {
            _diagnosticMessageSink = diagnosticMessageSink ?? throw new ArgumentNullException(nameof(diagnosticMessageSink));
            _diagnosticMessageSink.OnMessage(new Xunit.Sdk.DiagnosticMessage("F1.ctor()"));
        }

        public string Id { get; private set; } = Guid.NewGuid().ToString("N");

        public void Dispose()
        {
            _diagnosticMessageSink.OnMessage(new Xunit.Sdk.DiagnosticMessage("F1.dispose()"));
            Id = "aaaa";
        }
    }
    public class Test1_1 : IClassFixture<F1>
    {
        private readonly ITestOutputHelper _output;

        public Test1_1(ITestOutputHelper output, F1 f1)
        {
            _output = output ?? throw new ArgumentNullException(nameof(output));
            _output.WriteLine($"Test1_1({f1.Id})");
        }
        [Fact]
        public void Test1_1_Test()
        {
            Console.WriteLine("Console:Test1_1.Test1_1_Test()");
            _output.WriteLine("Output:Test1_1.Test1_1_Test()");
        }
    }
}
