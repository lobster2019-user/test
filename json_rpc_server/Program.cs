﻿using StreamJsonRpc;
using System;
using System.Net;
using System.Threading.Tasks;

namespace json_rpc_server
{
    class Program
    {
        private Program()
        {

        }
        [JsonRpcMethod("Add")]
        public Task<Int32> AddAsync(Int32 a, Int32 b)
        {
            Console.WriteLine($"Add ({a} + {b}) = {a + b}");
            return Task<Int32>.FromResult(a + b);
        }
        static async Task Main(string[] args)
        {
            Console.Title = "json_rpc_server";
            var http = new HttpListener();
            http.Prefixes.Add("http://localhost:39993/jsonrpc/");
            http.Start();
            Console.WriteLine("new started");
            while (true)
            {
                var context = await http.GetContextAsync();
                try
                {
                    var webSocketContext = await context.AcceptWebSocketAsync(null);
                    Console.WriteLine("new connect");
                    var task = Task.Factory.StartNew(async () =>
                    {
                        try
                        {
                            using (var ws = webSocketContext.WebSocket)
                            {
                                using (var jsonrpc = new JsonRpc(new WebSocketMessageHandler(ws)))
                                {
                                    jsonrpc.TraceSource.Switch.Level = System.Diagnostics.SourceLevels.Verbose;
                                    jsonrpc.TraceSource.Listeners.Add(new System.Diagnostics.TextWriterTraceListener(Console.Out));
                                    jsonrpc.AddLocalRpcTarget(new Program());
                                    jsonrpc.StartListening();
                                    await jsonrpc.Completion;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                        }
                        Console.WriteLine("disconnected");
                    });
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
        }
    }
}
