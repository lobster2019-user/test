using Microsoft.AspNetCore.Http.Connections.Client;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.DependencyInjection;
using multihub_server;
using NUnit.Framework;
using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;

namespace Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }
        Memory<byte> ToMemory(string text, Int32 offset = 0, Int32 length = -1)
        {
            return new Memory<byte>(
                System.Text.Encoding.UTF8.GetBytes(text, 0, length == -1 ? text.Length - offset : length));
        }
        [Test]
        public async Task TestRawClientAsync()
        {
            using (var ws = new ClientWebSocket())
            {

                ws.Options.Proxy = new WebProxyForce(new WebProxy("http://127.0.0.1:8888/"));
                await ws.ConnectAsync(
                                 uri: new Uri("ws://rabbitmq:6565/hubs/hello"),
                   cancellationToken: CancellationToken.None);

                var text0 = "{protocol:'json',version:1}\x1e";
                var text1 = "{invocationId:'1',type:1, target:'invoke',arguments :[ 'hello' ]}\x1e";
                var text2 = "{invocationId:'2',type:1, target:'invoke',arguments :[ 'hello' ]}\x1e";

                await ws.SendAsync(ToMemory(text0), WebSocketMessageType.Text, true, CancellationToken.None);
                

                await ws.SendAsync(ToMemory(text1 + text2.Substring(0, 10)), WebSocketMessageType.Text, true, CancellationToken.None);
				await Task.Delay(5000);
				
                await ws.SendAsync(ToMemory(text2.Substring(10)), WebSocketMessageType.Text, true, CancellationToken.None);


                var mem = new Memory<byte>(new byte[1024]);
                var read = await ws.ReceiveAsync(mem, new CancellationTokenSource(5000).Token);
                Debug.WriteLine(System.Text.Encoding.UTF8.GetString(mem.ToArray(), 0, read.Count));
                read = await ws.ReceiveAsync(mem, new CancellationTokenSource(5000).Token);
                Debug.WriteLine(System.Text.Encoding.UTF8.GetString(mem.ToArray(), 0, read.Count));

                await ws.CloseAsync(WebSocketCloseStatus.NormalClosure, "", CancellationToken.None);

                new ReadOnlyMemory<byte>();
            }
            //Assert.Pass();
        }

        [Test]
        public async Task TestClient()
        {
            HttpClientHandler handler = new HttpClientHandler();
            handler.Proxy = new WebProxy("http://127.0.0.1:8888/", BypassOnLocal: false);

            var builder = new HubConnectionBuilder()
           .WithUrl("http://rabbitmq:6565/hubs/hello")

           .ConfigureLogging(z =>
           {
               //z.AddConsole().SetMinimumLevel(LogLevel.Trace);
           });
            builder.Services.Configure<HttpConnectionOptions>(Options =>
            {
                Options.SkipNegotiation = true;
                Options.Transports = Microsoft.AspNetCore.Http.Connections.HttpTransportType.WebSockets;
                Options.Proxy = handler.Proxy;
                Options.HttpMessageHandlerFactory = (HttpMessageHandler o) =>
                {
                    //((HttpClientHandler)o).UseProxy = false;
                    return o;
                };
                Options.WebSocketConfiguration = o =>
                {
                    o.AddSubProtocol("jsonrpc");
                };
            });


            var connection = builder.Build();

            //Console.WriteLine(proxy);
            await connection.StartAsync();
            var ev = new AutoResetEvent(false);


            connection.On<string>("OnMessage", z =>
            {
                Console.WriteLine("on message: {0}", z);
                ev.Set();
            });
            var result = await connection.InvokeAsync<string>(
                       methodName: "Invoke",
                             arg1: "hello-text",
                cancellationToken: CancellationToken.None);

            Debug.WriteLine("invoke-async: {0}", result);
            await connection.SendAsync("Invoke", "hello-text34", cancellationToken: CancellationToken.None);
            Debug.WriteLine("send-async called");
        }

        [Test]
        public async Task TestMultiClient()
        {
            HttpClientHandler handler = new HttpClientHandler();
            handler.Proxy = new WebProxy("http://127.0.0.1:8888/", BypassOnLocal: false);

            var builder = new HubConnectionBuilder()
           .WithUrl(HubContract.Uri.ToString().Replace("127.0.0.1","rabbitmq"))

           .ConfigureLogging(z =>
           {
               //z.AddConsole().SetMinimumLevel(LogLevel.Trace);
           });
            builder.Services.Configure<HttpConnectionOptions>(Options =>
            {
                Options.SkipNegotiation = true;
                Options.Transports = Microsoft.AspNetCore.Http.Connections.HttpTransportType.WebSockets;
                Options.Proxy = handler.Proxy;
                Options.HttpMessageHandlerFactory = (HttpMessageHandler o) =>
                {
                    //((HttpClientHandler)o).UseProxy = false;
                    return o;
                };
            });


            var connection = builder.Build();

            //Console.WriteLine(proxy);
            await connection.StartAsync();
            var ev = new AutoResetEvent(false);


            connection.On<string>("IHello1Client_OnMessage", z =>
            {
                Console.WriteLine("on message: {0}", z);
                ev.Set();
            });
            await connection.SendAsync("Hello1Hub_OnMessage", "hellohub1", cancellationToken: CancellationToken.None);
            Console.WriteLine("send-async called");
            ev.WaitOne(5000);

            await connection.DisposeAsync();
        }
    }
}