﻿// Learn more about F# at http://fsharp.org

open System

let getEventNumbers = Seq.filter(fun number -> number%2 = 0)
let square = Seq.map(fun x -> x*x)

[<EntryPoint>]
let main argv =
    for n in getEventNumbers >> square <| seq { 0 .. 3 .. 27 } do 
        printfn "%d" n
    printfn "end"
    0 // return an integer exit code
