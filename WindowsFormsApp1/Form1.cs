﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using Vanara.PInvoke;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Vanara.PInvoke.User32.WINEVENTPROC _winEventProc;

        private async void Form1_Load(object sender, EventArgs e)
        {
            var taskbar = Microsoft.WindowsAPICodePack.Taskbar.TaskbarManager.Instance;
            await Task.Delay(1000);
            var newIcon = Properties.Resources.Icon2;
            using (var bmp = new Bitmap(this.Icon.Width, this.Icon.Height))
            {
                using (var gr = Graphics.FromImage(bmp))
                {
                    gr.DrawIcon(Icon, 0, 0);
                    var rect = new Rectangle(0, 0, Properties.Resources.Icon1.Width, Properties.Resources.Icon1.Height);
                    ColorMatrix colormatrix = new ColorMatrix();
                    colormatrix.Matrix33 = 0.8f;
                    ImageAttributes imgAttribute = new ImageAttributes();
                    imgAttribute.SetColorMatrix(colormatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
                    gr.DrawImage(Icon.ToBitmap(), new System.Drawing.Point(0, 0));

                    gr.DrawImage(newIcon.ToBitmap(), new Rectangle(
                        0 * (Int32)(bmp.Width / 3.0f),
                        0,
                        (Int32)(3f * bmp.Width / 3f),
                        (Int32)(3f * bmp.Height / 3f)),
                        0,
                        0,
                        Properties.Resources.Icon2.Width,
                        Properties.Resources.Icon2.Height,
                        GraphicsUnit.Pixel, imgAttribute);
                }
                this.Icon = Icon.FromHandle(bmp.GetHicon());
                var notepad = System.Diagnostics.Process.Start("notepad.exe");
                System.Threading.Thread.Sleep(5000);
                var mainhwnd = notepad.MainWindowHandle;
                IntPtr CustomMenuID = new IntPtr(888777000);
                var hmenu = Vanara.PInvoke.User32_Gdi.GetMenu(
                    mainhwnd);
                User32_Gdi.InsertMenu(
                    hmenu, UInt32.MaxValue,
                    User32_Gdi.MenuFlags.MF_BYPOSITION,
                    CustomMenuID,
                    "new item");
                User32_Gdi.DrawMenuBar(mainhwnd);

                var EVENT_OBJECT_INVOKED = 0x8013u;

                _winEventProc = new User32.WINEVENTPROC(
                    (User32.HWINEVENTHOOK hWinEventHook,
                    uint winEvent,
                    HWND hwnd,
                    int idObject,
                    int idChild,
                    uint idEventThread,
                    uint dwmsEventTime) =>
                    {
                        
                        if (idChild == (Int32)CustomMenuID)
                        {
                            var w = new NativeWindow();
                            w.AssignHandle(mainhwnd);
                            var w2 = new Window();
                            new System.Windows.Interop.WindowInteropHelper(w2).Owner = mainhwnd;
                            w2.Content = "hello";
                            w2.ShowDialog();
                        }

                    });
                var winEventHook = User32.SetWinEventHook(
                        EVENT_OBJECT_INVOKED,
                        EVENT_OBJECT_INVOKED,
                        IntPtr.Zero,
                        _winEventProc, 0, 0,
                        User32.WINEVENT.WINEVENT_OUTOFCONTEXT);

            }

        }
    }
}
