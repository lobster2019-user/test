﻿using System;
using System.Buffers;
using System.Text;

namespace Tests
{
    public static class TextDecoderExtensions
    {
        public static Int32 Convert(this Decoder decoder,
                       ReadOnlySequence<byte> bytes,
                       Span<char> chars,
                       bool flush,
                       out SequencePosition consumed)

        {
            consumed = bytes.Start;
            Int32 decoded = 0;
            foreach (var srcSegment in bytes)
            {
                decoder.Convert(
                    srcSegment.Span,
                    chars,
                    flush: flush,
                    out var bytesUsed,
                    out var charsUsed,
                    out var _);

                decoded += charsUsed;
                consumed = bytes.GetPosition(bytesUsed, consumed);
                chars = chars.Slice(charsUsed);

                if (chars.Length == 0)
                {
                    break;
                }
            }
            return decoded;
        }
    }
}