﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace test_netcore_app
{
    [Route("api/[controller]")]
    [ApiController]
    public class HelloController : ControllerBase
    {
        [HttpGet("{text}")]
        public async Task<ActionResult<object>> Get(string text)
        {
            return new { Result = text };
        }
        [HttpGet("hello2/{text}")]
        public async Task<string> Get2(string text)
        {
            return text;
        }
    }

}
