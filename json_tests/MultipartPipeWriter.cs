﻿using System;
using System.Buffers;
using System.IO.Pipelines;
using System.Threading.Tasks;

namespace Tests
{
    public class MultipartPipeWriter
    {
        private readonly byte[] _text;
        private readonly PipeWriter _writer;
        private readonly int _size;

        public MultipartPipeWriter(
            string text,
            PipeWriter writer, Int32 size)
        {
            _text =System.Text.Encoding.UTF8.GetBytes(text);
            _writer = writer;
            _size = size;
        }
        public async Task StartAsync()
        {
            var pos = 0;
            while (pos < _text.Length)
            {
                var text = new Memory<byte>( _text,pos, Math.Min(_size, _text.Length - pos));
                await WriteAsync(text);
                Console.WriteLine($"write: {text}");
                await Task.Delay(2000);
                pos += _size;
            }
            _writer.Complete();
        }

        async Task WriteAsync(Memory<byte> d)
        {
            var memory = _writer.GetMemory(d.Length);
            d.CopyTo(memory);
            _writer.Advance(d.Length);
            await _writer.FlushAsync();
        }
    }
}