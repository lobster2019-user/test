using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System;
using System.IO.Pipelines;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Tests
{

    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public async Task Test1()
        {
            var str = "{'a':'b','c':'d�'}{'v':'k','g':'h1'}{'g':'b'}";
            var pipe = new Pipe();
            var writer = new MultipartPipeWriter(str, pipe.Writer, 16);
            writer.StartAsync();

            var jsonReader = new JsonTextReader(
              new PipeTextReader(pipe.Reader, encoding: Encoding.UTF8))
            {
                SupportMultipleContent = true,
            };
            Console.WriteLine();
            while (await jsonReader.ReadAsync())
            {
                if (jsonReader.TokenType == JsonToken.StartObject)
                {
                    var v = await JToken.ReadFromAsync(jsonReader);
                    Console.WriteLine(v.Type);
                    Console.WriteLine(v.ToString());
                }
                else
                {
                    Console.WriteLine(jsonReader.TokenType);
                }
            }
            Console.WriteLine();

        }
    }
}